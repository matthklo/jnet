#pragma once

#include "platform.h"
#include "mutex.h"

/* A synchronous FIFO */

typedef struct _JNFifoRecord
{
	void*                 data;
	struct _JNFifoRecord* prev;
	struct _JNFifoRecord* next;
} JNFifoRecord;

typedef struct _JNFifo
{
	const JNContext*    context;
	JNFifoRecord*       head;
	JNFifoRecord*       end;
	unsigned int        size;
	JNMutex*            mutex;
} JNFifo;

typedef void (*custom_free_func)(void*);

JNFifo*
jn_fifo_create(const JNContext *context);

void
jn_fifo_destroy(JNFifo *fifo, custom_free_func free_func);

// Peek and return the first element of fifo without popping it. NULL if empty.
void*
jn_fifo_front(JNFifo *fifo);

// Pop and return the first element of fifo. NULL if empty.
void*
jn_fifo_popfront(JNFifo *fifo);

// Peek and return the last element of fifo without popping it. NULL if empty.
void*
jn_fifo_back(JNFifo *fifo);

// Return the size of fifo after push
unsigned int
jn_fifo_pushback(JNFifo *fifo, void *data);

// Iterate through all records in the fifo, passing each record->data to the
// predictor function 'pred'. And remove those records whose return value of 
// predictor function is 1.
// Return the number of records has been removed.
// Note: O(N)
typedef int (*predictor_func)(void *data, void *param);
int
jn_fifo_remove(JNFifo *fifo, predictor_func pred, void *pred_param);

