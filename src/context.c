#include "platform.h"
#include <jnet/jnet.h>
#include <jnet/context.h>
#include <stdlib.h>

// Platform-dependent global initialize() / terminate() functions.
#ifdef JN_PLATFORM_WINDOWS
#  include <Winsock2.h>
#  pragma comment(lib, "ws2_32.lib")

static int _g_global_initialized = 0;
int
jn_initialize()
{
	WORD wVersionRequested;
	WSADATA wsaData;
	int ec;

	if (0 != _g_global_initialized)
		return 0;

	wVersionRequested = MAKEWORD(2, 2);
	ec = WSAStartup(wVersionRequested, &wsaData);
	if (ec != 0)
		return 1;

	_g_global_initialized = 1;
	return 0;
}

void
jn_terminate()
{
	if (_g_global_initialized)
	{
		WSACleanup();
		_g_global_initialized = 0;
	}
}

#else

int
jn_initialize()
{
	// nothing really.
	return 0; 
}

void
jn_terminate()
{
	// nothing really.
}

#endif


// Default logger - just ignore all.
static void _default_logger(JNELogLevel level, const char *msg)
{
}


JNContext*
jn_context_create()
{
	// NOTE: ALWAYS use standard malloc/free on context structures.
	JNContext* ctx = (JNContext*)malloc(sizeof(JNContext));
	if (NULL != ctx)
	{
		// Assign default memory operators.
		// TODO: switch to jemalloc
		ctx->free = free;
		ctx->malloc = malloc;
		ctx->realloc = realloc;

		// Assign default logger.
		ctx->logger = _default_logger;
	}

	return ctx;
}


int
jn_context_destroy(JNContext* context)
{
	// NOTE: ALWAYS use standard malloc/free on context structures.
	if (context)
	{
		free(context);
	}
	return 0;
}

