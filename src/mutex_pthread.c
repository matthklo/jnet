#include "platform.h"
#include "mutex.h"

// Use pthread-implementation on all platforms except Windows
#if !defined(JN_PLATFORM_WINDOWS)
#include <pthread.h>

JNMutex*
jn_mutex_create(const JNContext *ctx)
{
	if (!ctx)
		return NULL;

	JNMutex *m = (JNMutex*)ctx->malloc(sizeof(JNMutex));
	if (m)
	{
		m->context = ctx;
		pthread_mutex_t *pm = (pthread_mutex_t*)ctx->malloc(sizeof(pthread_mutex_t));
		if (!pm)
		{
			ctx->free(m);
			return NULL;
		}
		pthread_mutexattr_t attr;
		pthread_mutexattr_init(&attr);
		pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);
		pthread_mutex_init(pm, &attr);
		pthread_mutexattr_destroy(&attr);
		m->opaque = pm;
	}
	return m;
}

int
jn_mutex_lock(JNMutex *mutex)
{
	return (0 == pthread_mutex_lock((pthread_mutex_t*)mutex->opaque)) ? 1 : 0;
}

int
jn_mutex_trylock(JNMutex *mutex)
{
	return (0 == pthread_mutex_trylock((pthread_mutex_t*)mutex->opaque)) ? 1 : 0;
}

int
jn_mutex_unlock(JNMutex *mutex)
{
	return (0 == pthread_mutex_unlock((pthread_mutex_t*)mutex->opaque)) ? 1 : 0;
}

int
jn_mutex_destroy(JNMutex *mutex)
{
	if (mutex)
	{
		if (mutex->opaque)
		{
			pthread_mutex_destroy((pthread_mutex_t*)mutex->opaque);
			mutex->context->free(mutex->opaque);
		}
		mutex->context->free(mutex);
	}
	return 1;
}
#endif
