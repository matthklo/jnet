#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include "platform.h"
#include <jnet/addr.h>

#ifdef JN_PLATFORM_WINDOWS
#include <WS2tcpip.h>
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#endif

#include <stdio.h>
#include <string.h>

void
jn_address_clear(JNAddress *addr)
{
	memset(addr, 0, sizeof(JNAddress));
}

void
jn_address_clone(JNAddress *dst, const JNAddress *src)
{
	memcpy(dst, src, sizeof(JNAddress));
}

void
jn_address_load_port(JNAddress *addr)
{
	if (addr->socklen == sizeof(struct sockaddr_in))
	{
		struct sockaddr_in *s = (struct sockaddr_in *)addr->sockaddr;
		addr->port = ntohs(s->sin_port);
	}
	else if (addr->socklen == sizeof(struct sockaddr_in))
	{
		struct sockaddr_in6 *s = (struct sockaddr_in6 *)addr->sockaddr;
		addr->port = ntohs(s->sin6_port);
	}
	else
		addr->port = 0;
}

JNEErrCodes
jn_address_set(JNAddress *addr, const char *host, unsigned short port, unsigned short flags)
{
	struct addrinfo hint;
	struct addrinfo *results = NULL;

	if (!addr)
		return JN_EC_INVALID_PARAMS;

	memset(&hint, 0, sizeof(struct addrinfo));
	hint.ai_family = AF_UNSPEC;
	hint.ai_socktype = SOCK_STREAM;
	hint.ai_flags = AI_ADDRCONFIG;
	if (flags & JN_ADDRESS_FLAG_PASSIVE)
		hint.ai_flags |= AI_PASSIVE;
#ifndef JN_PLATFORM_ANDROID
	hint.ai_flags |= AI_V4MAPPED;
#endif

	char portbuf[64] = {0};
	sprintf(portbuf, "%u", port);

	if (0 != getaddrinfo(host, portbuf, &hint, &results))
		return JN_EC_PLATFORM_ERROR;

	// After getaddrinfo() success, need to iterate over the result list
	// and pick out the proper setting.
	// By default, the AF_INET (IPv4) is prefered. However, user can use
	// JN_ADDRESS_FLAG_IPV6 to set the preference of AF_INET6 (IPv6).
	int filter[2] = { AF_INET, AF_INET6 };
	if (flags & JN_ADDRESS_FLAG_IPV6)
	{
		filter[0] = AF_INET6;
		filter[1] = AF_INET;
	}
	
	int i;
	for (i=0; i< sizeof(filter)/sizeof(filter[0]); ++i)
	{
		struct addrinfo *pr = results;
		while (pr != NULL)
		{
			if ((filter[i] == pr->ai_family) && (pr->ai_addrlen <= JN_SOCKADDR_MAX_LENGTH))
			{
				addr->socklen = (unsigned int)pr->ai_addrlen;
				memcpy(addr->sockaddr, pr->ai_addr, addr->socklen);
				addr->port = port;
				freeaddrinfo(results);
				return JN_EC_OK;
			}
			pr = pr->ai_next;
		}
	}
	if (results)
		freeaddrinfo(results);
	return JN_EC_NO_MATCH;
}

JNEErrCodes
jn_address_get_hostname(const JNAddress *addr, char *buf, int *length, unsigned short flags)
{
	int ec;
	ec = getnameinfo((const struct sockaddr*)addr->sockaddr, (socklen_t)addr->socklen
			, buf, (size_t)*length
			, NULL, 0
			, (flags & JN_ADDRESS_FLAG_NUMERIC) ? NI_NUMERICHOST : 0);

	switch (ec)
	{
	case 0:
		*length = (int)strlen(buf);
		return JN_EC_OK;
	case EAI_MEMORY:
#ifdef JN_PLATFORM_LINUX
	case EAI_OVERFLOW:
#endif
		return JN_EC_OUT_OF_MEMORY;
	default:
		break;
	}
	return JN_EC_PLATFORM_ERROR;
}


JNEIPProtocolVersion
jn_address_tell_ipver(const JNAddress *addr)
{
	if (addr)
	{
		if (addr->socklen == sizeof(struct sockaddr_in))
			return JN_IPPROTO_IPV4;
		else if (addr->socklen == sizeof(struct sockaddr_in6))
			return JN_IPPROTO_IPV6;
	}
	return JN_IPPROTO_INVALID;
}
