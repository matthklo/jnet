#pragma once
#include <jnet/context.h>


typedef struct _JNMutex
{
	const JNContext* context;
	void*            opaque;
} JNMutex;


// Create a mutex object.
JNMutex*
jn_mutex_create(const JNContext *ctx);

// Acquire the lock of a mutex. Block until acquired.
// Return value: 
//    non-zero: succeed.
//    zero: failed.
int
jn_mutex_lock(JNMutex *mutex);

// Acquire the lock of a mutex without blocking.
// Return value: 
//    non-zero: succeed.
//    zero: failed.
int
jn_mutex_trylock(JNMutex *mutex);

// Release the lock of the mutex.
// Return value:
//    non-zero: succeed.
//    zero: failed (not own the lock).
int
jn_mutex_unlock(JNMutex *mutex);


// Destroy a mutex object.
// Return value:
//    always 1.
int
jn_mutex_destroy(JNMutex *mutex);

