#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include "platform.h"

/* IOCP (I/O Completion Port) is only available on Windows platform */
#ifdef JN_PLATFORM_WINDOWS
#include "asio.h"
#include "fifo.h"
#include <jnet/addr.h>
#include <jnet/peer.h>

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

typedef struct _JNOverlapped
{
	OVERLAPPED   overlapped;
	JNHostEvent  evt;
	JNSocket     involving_socket;
	JNSocket     accepted_socket;
	WSABUF       wsabuf;
} JNOverlapped;

static void _log(const JNContext *ctx, JNELogLevel level, const char *fmt, ...)
{
	va_list argptr;
	va_start(argptr, fmt);

	char buf[1024];
	vsprintf(buf, fmt, argptr);
	ctx->logger(level, buf);

	va_end(argptr);
}

// Return 1: Succeed. 0: Failed.
static int _load_acceptex_funcptr(JNSocket socket, LPFN_ACCEPTEX *outptr)
{
	GUID GuidAcceptEx = WSAID_ACCEPTEX;
	DWORD dwBytes = 0;
	int ec = WSAIoctl(socket, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidAcceptEx, sizeof(GuidAcceptEx),
		outptr, sizeof(LPFN_ACCEPTEX),
		&dwBytes, NULL, NULL);
	return (SOCKET_ERROR == ec) ? 0 : 1;
}

// Return 1: Succeed. 0: Failed.
static int _load_connectex_funcptr(JNSocket socket, LPFN_CONNECTEX *outptr)
{
	GUID GuidConnectEx = WSAID_CONNECTEX;
	DWORD dwBytes = 0;
	int ec = WSAIoctl(socket, SIO_GET_EXTENSION_FUNCTION_POINTER,
		&GuidConnectEx, sizeof(GuidConnectEx),
		outptr, sizeof(LPFN_CONNECTEX),
		&dwBytes, NULL, NULL);
	return (SOCKET_ERROR == ec) ? 0 : 1;
}

JNAsio*
jn_asio_create(const JNContext *context)
{
	JNAsio *asio = (JNAsio*)context->malloc(sizeof(JNAsio));
	do
	{
		if (!asio)
			break;

		memset(asio, 0, sizeof(JNAsio));
		asio->context = context;

		asio->iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 0 /*num of concurrent threads*/);
		if (NULL == asio->iocp)
		{
			DWORD le = GetLastError();
			_log(context, JN_LL_ERROR, "jn_asio_create: Failed on CreateIoCompletionPort. GetLastError(): %u", le);
			context->free(asio);
			asio = NULL;
			break;
		}

		asio->udp_sends = jn_fifo_create(context);
		if (!asio->udp_sends)
		{
			_log(context, JN_LL_ERROR, "jn_asio_create: Failed on initialize internal data.");
			if (asio->udp_sends)
				jn_fifo_destroy(asio->udp_sends, context->free);
			context->free(asio);
			asio = NULL;
			break;
		}
		
	} while (0);
	return asio;
}

JNEErrCodes
jn_asio_shutdown(JNAsio *asio)
{
	asio->bShutdown = 1;
	return JN_EC_OK;
}

JNEErrCodes
jn_asio_destroy(JNAsio *asio)
{
	CloseHandle(asio->iocp);
	jn_fifo_destroy(asio->udp_sends, asio->context->free);
	asio->context->free(asio);
	return JN_EC_OK;
}

static JNEErrCodes
_asio_deliver_job_tcp(JNAsio *asio, JNEHostType htype, JNHostEvent *in_evt)
{
	JNEErrCodes ec = JN_EC_OK;
	JNPeer *peer = in_evt->peer;
	JNHost *host = peer->host;
	const JNContext *context = asio->context;

	int delete_overlapped = 1;
	JNOverlapped *o = (JNOverlapped*)context->malloc(sizeof(JNOverlapped));
	memset(&o->overlapped, 0, sizeof(OVERLAPPED));
	memcpy(&o->evt, in_evt, sizeof(JNHostEvent));
	o->evt.ec = JN_EC_IN_PROGRESS;
	o->accepted_socket = INVALID_SOCKET;
	o->involving_socket = INVALID_SOCKET;

	switch (in_evt->type)
	{
	case JN_EVENT_ACCEPT:
	case JN_EVENT_RECV:
		if (peer->asio_data->rdata)
		{
			context->free(o);
			return JN_EC_IN_PROGRESS; //another async operation is in progress.
		}
		peer->asio_data->rdata = o;
		break;
	case JN_EVENT_DISCONNECT:
		break;
	default:
		if (peer->asio_data->wdata)
		{
			context->free(o);
			return JN_EC_IN_PROGRESS; //another async operation is in progress.
		}
		peer->asio_data->wdata = o;
		break;
	}

	switch (in_evt->type)
	{
	case JN_EVENT_ACCEPT:
		do
		{
			int      is_ipv6;
			JNSocket accepted_socket;
			DWORD    addrlen;
			DWORD    bytes_received;

			// If the function pointer to AcceptEx() has not yet accquired. Accquire it.
			if (NULL == asio->pAcceptEx)
			{
				if (!_load_acceptex_funcptr(host->hostpeer->socket, &asio->pAcceptEx))
				{
					context->logger(JN_LL_ERROR, "jn_asio_deliver_job: Failed on accquiring AcceptEx function pointer.");
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}
			}

			// Prepare the necessary stuff for AcceptEx().
			// A major diff between AcceptEx() and the traditional accept() is that the socket
			// dedicated to the accepted connection should be prepared before calling AcceptEx().
			is_ipv6 = (host->hostpeer->localaddr.socklen == sizeof(struct sockaddr_in6));
			accepted_socket = socket(
				is_ipv6 ? AF_INET6 : AF_INET,
				SOCK_STREAM, IPPROTO_TCP);
			if (INVALID_SOCKET == accepted_socket)
			{
				context->logger(JN_LL_ERROR, "Failed on preparing accept_socket.");
				ec = JN_EC_PLATFORM_ERROR;
				break;
			}
			addrlen = is_ipv6 ? sizeof(struct sockaddr_in6) : sizeof(struct sockaddr_in);
			addrlen += 16; // required by AcceptEx()
			bytes_received = 0; // required by AcceptEx(). but its value are not used.
			o->accepted_socket = accepted_socket;
			o->involving_socket = host->hostpeer->socket;

			BOOL result = asio->pAcceptEx(host->hostpeer->socket, accepted_socket, o->evt.addr.sockaddr, 0, addrlen, addrlen, &bytes_received, (OVERLAPPED*)o);
			if (result == TRUE || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				// the job has been issued. wait for completion event.
				delete_overlapped = 0;
			}
			else
			{
				// something went wrong, post an completion event address for this error.
				// TODO: Does AcceptEx() also post completion event under such circumstance?
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();
				o->accepted_socket = INVALID_SOCKET;
				closesocket(accepted_socket);

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a fail accept. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		} while (0);
		break;

	case JN_EVENT_CONNECT:
		do
		{
			//int is_ipv6;
			//int bind_ec;

			// If the function pointer to ConnectEx() has not yet accquired. Accquire it.
			if (NULL == asio->pConnectEx)
			{
				if (!_load_connectex_funcptr(host->hostpeer->socket, &asio->pConnectEx))
				{
					context->logger(JN_LL_ERROR, "jn_asio_deliver_job: Failed on accquiring ConnectEx function pointer.");
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}
			}

			o->involving_socket = host->hostpeer->socket;

			peer->state = JN_PEER_STATE_CONNECTING;
			BOOL result = asio->pConnectEx(host->hostpeer->socket,
				(const struct sockaddr*)o->evt.addr.sockaddr, (int)o->evt.addr.socklen,
				0, 0, 0, (LPWSAOVERLAPPED)o);
			if (result == TRUE || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				// the job has been issued. wait for completion event.
				delete_overlapped = 0;
			}
			else
			{
				// something went wrong, post an completion event address for this error.
				// TODO: Does ConnectEx() also post completion event under such circumstance?
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a fail connect. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		} while (0);
		break;

	case JN_EVENT_RECV:
		{
			int recv_ec;
			DWORD dummy_flags = 0; // currently not used.

			o->wsabuf.buf = o->evt.in_data;
			o->wsabuf.len = o->evt.data_len;
			o->involving_socket = peer->socket;

			recv_ec = WSARecv(peer->socket, &o->wsabuf, 1, NULL, &dummy_flags,
				(LPWSAOVERLAPPED)o, 0);
			if ((0 == recv_ec) || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				delete_overlapped = 0;
			}
			else // error
			{
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a recv error. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		}
		break;

	case JN_EVENT_SEND:
		{
			int send_ec;

			o->wsabuf.buf = (char*)o->evt.out_data;
			o->wsabuf.len = o->evt.data_len;
			o->involving_socket = peer->socket;

			send_ec = WSASend(peer->socket, &o->wsabuf, 1, NULL, 0,
				(LPWSAOVERLAPPED)o, 0);
			if ((0 == send_ec) || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				delete_overlapped = 0;
			}
			else // error
			{
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a send error. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		}
		break;

	case JN_EVENT_DISCONNECT:
		jn_asio_cancel_socket_io(asio, peer->socket);
		o->evt.ec = JN_EC_OK;
		peer->state = JN_PEER_STATE_DISCONNECTING;
		if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
		{
			DWORD lastec = GetLastError();
			_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a disconnect event. GetLastError(): %u", lastec);
			ec = JN_EC_PLATFORM_ERROR;
			break;
		}
		delete_overlapped = 0;
		break;

	default:
		ec = JN_EC_INVALID_PARAMS;
		break;
	}

	if (delete_overlapped)
	{
		switch (in_evt->type)
		{
		case JN_EVENT_ACCEPT:
		case JN_EVENT_RECV:
			peer->asio_data->rdata = NULL;
			break;
		case JN_EVENT_DISCONNECT:
			break;
		default:
			peer->asio_data->wdata = NULL;
			break;
		}

		context->free(o);
		o = NULL;
	}

	return ec;
}

static JNEErrCodes
_asio_deliver_job_udp(JNAsio *asio, JNEHostType htype, JNHostEvent *in_evt)
{
	JNEErrCodes ec = JN_EC_OK;
	JNPeer *peer = in_evt->peer;
	JNHost *host = peer->host;
	const JNContext *context = asio->context;

	int delete_overlapped = 1;
	JNOverlapped *o = (JNOverlapped*)context->malloc(sizeof(JNOverlapped));
	memset(&o->overlapped, 0, sizeof(OVERLAPPED));
	memcpy(&o->evt, in_evt, sizeof(JNHostEvent));
	o->evt.ec = JN_EC_IN_PROGRESS;
	o->accepted_socket = INVALID_SOCKET;
	o->involving_socket = INVALID_SOCKET;

	switch (in_evt->type)
	{
	case JN_EVENT_RECV:
		{
			int recvfrom_ec;
			DWORD dummy_flags = 0;

			if (peer->asio_data->rdata)
			{
				context->free(o);
				return JN_EC_IN_PROGRESS; //another async operation is in progress.
			}
			peer->asio_data->rdata = o;

			o->wsabuf.buf = o->evt.in_data;
			o->wsabuf.len = o->evt.data_len;
			o->involving_socket = host->hostpeer->socket;

			recvfrom_ec = WSARecvFrom(host->hostpeer->socket, &o->wsabuf, 1, NULL, &dummy_flags,
				(struct sockaddr*)o->evt.addr.sockaddr, &o->evt.addr.socklen,
				(LPOVERLAPPED)o, 0);
			if ((0 == recvfrom_ec) || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				delete_overlapped = 0;
			}
			else // error
			{
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a recvfrom error. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		}
		break;

	case JN_EVENT_SEND:
		{
			int sendto_ec = 0;

			o->wsabuf.buf = (char*)o->evt.out_data;
			o->wsabuf.len = o->evt.data_len;
			o->involving_socket = host->hostpeer->socket;

			jn_mutex_lock(host->hostpeer->asio_data->mutex);
			unsigned int cnt = jn_fifo_pushback(asio->udp_sends, o);
			if (cnt == 1) // this packet is the only outgoing packet in progress. invoke WSASend now.
			{
				peer->asio_data->wdata = o;
				sendto_ec = WSASendTo(host->hostpeer->socket, &o->wsabuf, 1, 0, 0,
					(const struct sockaddr*)o->evt.addr.sockaddr, (int)o->evt.addr.socklen,
					(LPOVERLAPPED)o, 0);
			}
			jn_mutex_unlock(host->hostpeer->asio_data->mutex);

			if ((0 == sendto_ec) || (ERROR_IO_PENDING == WSAGetLastError()))
			{
				delete_overlapped = 0;
			}
			else // error
			{
				o->evt.ec = JN_EC_PLATFORM_ERROR;
				o->evt.platform_ec = WSAGetLastError();

				if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
				{
					// If fails on posting an error completion event, throw a log and return error code.
					DWORD lastec = GetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_deliver_job: Failed on PostQueuedCompletionStatus() when reporting a sendto error. GetLastError(): %u", lastec);
					ec = JN_EC_PLATFORM_ERROR;
					break;
				}

				delete_overlapped = 0;
			}
		}
		break;

	default:
		ec = JN_EC_INVALID_PARAMS;
		break;
	}

	if (delete_overlapped)
	{
		switch (in_evt->type)
		{
		case JN_EVENT_ACCEPT:
		case JN_EVENT_RECV:
			peer->asio_data->rdata = NULL;
			break;
		default:
			peer->asio_data->wdata = NULL;
			break;
		}

		context->free(o);
		o = NULL;
	}

	return ec;
}

JNEErrCodes
jn_asio_deliver_job(JNAsio *asio, JNHostEvent *in_evt)
{
	if (asio->bShutdown)
		return JN_EC_SHUTTING_DOWN;

	JNEHostType htype = in_evt->peer->host->type;
	switch (htype)
	{
	case JN_HOST_TCP_SERVER:
	case JN_HOST_TCP_CLIENT:
		return _asio_deliver_job_tcp(asio, htype, in_evt);
	case JN_HOST_UDP_SERVER:
	case JN_HOST_UDP_CLIENT:
		return _asio_deliver_job_udp(asio, htype, in_evt);
	default:
		break;
	}
	return JN_EC_INVALID_PARAMS;
}

static JNEErrCodes
_query_tcpudp_job_completion(JNAsio *asio, EventCallback cb, unsigned int interval_ms)
{
	DWORD bytes = 0;
	ULONG_PTR dummy;
	JNPeer *peer = NULL;
	JNHost *host = NULL;
	const JNContext *context = asio->context;
	JNOverlapped *overlapped = NULL;
	BOOL ret;

	// Dequeue completion event from IOCP.
	ret = GetQueuedCompletionStatus(asio->iocp, &bytes, &dummy, (LPOVERLAPPED*)&overlapped, interval_ms);
	if (asio->bShutdown)
		return JN_EC_SHUTTING_DOWN;
	if (FALSE == ret)
	{
		if (NULL == overlapped)
		{
			// IOCP does not dequeue any completion event.
			// Probably due to timeout or error of IOCP itself.
			DWORD lasterr = GetLastError();
			if (lasterr == ERROR_ABANDONED_WAIT_0) // The IOCP itself has been closed.
				return JN_EC_PLATFORM_ERROR;
			else
				return JN_EC_NO_MATCH;
		}
	}

	// Check if this is a completion event of asio instead of error completion events post manually.
	if (overlapped->evt.ec == JN_EC_IN_PROGRESS)
	{
		// Figure out the actual result.
		DWORD dummy; // not used.
		ret = WSAGetOverlappedResult(overlapped->involving_socket, (LPOVERLAPPED)overlapped, &bytes, TRUE, &dummy);
		if (TRUE == ret)
		{
			overlapped->evt.ec = JN_EC_OK;
			overlapped->evt.platform_ec = 0;
		}
		else
		{
			overlapped->evt.ec = JN_EC_PLATFORM_ERROR;
			overlapped->evt.platform_ec = WSAGetLastError(); // TODO: may not get the actual error in multithread case.
		}
	}

	// Handle the completion event and fire callback.
	peer = overlapped->evt.peer;
	host = peer->host;
	switch (host->type)
	{
	case JN_HOST_TCP_CLIENT:
	case JN_HOST_TCP_SERVER:
		switch (overlapped->evt.type)
		{
		case JN_EVENT_ACCEPT:
			if (overlapped->evt.ec == JN_EC_OK)
			{
				int setsockopt_ec;

				setsockopt_ec = setsockopt(overlapped->accepted_socket, SOL_SOCKET, SO_UPDATE_ACCEPT_CONTEXT,
					(char *)&overlapped->involving_socket, sizeof(void*));

				if (setsockopt_ec)
				{
					int le = WSAGetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_query_job_completion: Failed on calling setsockopt(SO_UPDATE_ACCEPT_CONTEXT) on accepted socket. WSAGetLastError:%d", le);
					closesocket(overlapped->accepted_socket);
					overlapped->accepted_socket = INVALID_SOCKET;
					overlapped->evt.ec = JN_EC_PLATFORM_ERROR;
					overlapped->evt.platform_ec = le;
				}
				else
				{
					// Create JNPeer for the newly accepted connection.
					JNPeer *p = (JNPeer*)context->malloc(sizeof(JNPeer));
					memset(p, 0, sizeof(JNPeer));
					p->host = host;
					p->socket = overlapped->accepted_socket;
					p->state = JN_PEER_STATE_CONNECTED;
					p->localaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
					p->remoteaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
					p->asio_data = jn_asio_peerdata_create(context);
					getsockname(p->socket, (struct sockaddr*)p->localaddr.sockaddr, (int*)&p->localaddr.socklen);
					getpeername(p->socket, (struct sockaddr*)p->remoteaddr.sockaddr, (int*)&p->remoteaddr.socklen);
					jn_address_load_port(&p->localaddr);
					jn_address_load_port(&p->remoteaddr);
					memcpy(&overlapped->evt.addr, &p->remoteaddr, sizeof(JNAddress));
					overlapped->evt.peer = p;

					// join asio
					jn_asio_join_socket(asio, p->socket);
				}
			}
			peer->asio_data->rdata = NULL;

			// Start next async accept
			if (host->hostpeer->state == JN_PEER_STATE_LISTENING)
			{
				JNHostEvent evt;
				evt.type = JN_EVENT_ACCEPT;
				evt.peer = host->hostpeer;
				jn_asio_deliver_job(asio, &evt);
			}
			break;

		case JN_EVENT_CONNECT:
			if (overlapped->evt.ec == JN_EC_OK)
			{
				int setsockopt_ec;

				setsockopt_ec = setsockopt(overlapped->involving_socket, SOL_SOCKET, SO_UPDATE_CONNECT_CONTEXT, NULL, 0);
				if (setsockopt_ec)
				{
					int le = WSAGetLastError();
					_log(context, JN_LL_ERROR, "jn_asio_query_job_completion: Failed on calling setsockopt(SO_UPDATE_CONNECT_CONTEXT) on connected socket. WSAGetLastError:%d", le);
					overlapped->evt.ec = JN_EC_PLATFORM_ERROR;
					overlapped->evt.platform_ec = le;
				}
				else
				{
					peer->state = JN_PEER_STATE_CONNECTED;
					overlapped->evt.peer = peer;
					peer->remoteaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
					getpeername(peer->socket, (struct sockaddr*)peer->remoteaddr.sockaddr, (int*)&peer->remoteaddr.socklen);
				}
			}
			peer->asio_data->wdata = NULL;
			break;

		case JN_EVENT_DISCONNECT:
			// Applicator should free the peer (via jn_peer_destroy) when receives JN_EVENT_DISCONNECT
			peer->state = JN_PEER_STATE_DISCONNECTED;
			closesocket(peer->socket);
			peer->socket = INVALID_SOCKET;
			peer->asio_data->wdata = NULL;
			break;

		case JN_EVENT_RECV:
			overlapped->evt.data_len = (unsigned int)bytes;
			peer->asio_data->rdata = NULL;
			break;
		case JN_EVENT_SEND:
			overlapped->evt.data_len = (unsigned int)bytes;
			peer->asio_data->wdata = NULL;
			break;
		default:
			break;
		}
		break;

	case JN_HOST_UDP_CLIENT:
	case JN_HOST_UDP_SERVER:
		switch (overlapped->evt.type)
		{
		case JN_EVENT_RECV:
			jn_address_load_port(&overlapped->evt.addr);
			overlapped->evt.data_len = (unsigned int)bytes;
			peer->asio_data->rdata = NULL;
			break;

		case JN_EVENT_SEND:
			{
				overlapped->evt.data_len = (unsigned int)bytes;

				//// Enter critical section
				jn_mutex_lock(peer->asio_data->mutex);

				JNOverlapped *o = (JNOverlapped *)jn_fifo_popfront(asio->udp_sends);
				if (o != overlapped)
				{
					// Fatal error.
					_log(context, JN_LL_ERROR, "jn_asio_query_job_completion: FATAL ERROR: outgoing overlapped event is not at the front of udp_sends.");
				}

				// If there are more outgoing events, invoke the next one.
				o = jn_fifo_front(asio->udp_sends);
				int sendto_ec = 0;
				peer->asio_data->wdata = o;
				if (o)
				{
					sendto_ec = WSASendTo(host->hostpeer->socket, &o->wsabuf, 1, 0, 0,
						(const struct sockaddr*)o->evt.addr.sockaddr, (int)o->evt.addr.socklen,
						(LPOVERLAPPED)o, 0);
				}

				jn_mutex_unlock(peer->asio_data->mutex);
				//// Leave critical section

				if ((0 != sendto_ec) && (ERROR_IO_PENDING != WSAGetLastError()))
				{
					o->evt.ec = JN_EC_PLATFORM_ERROR;
					o->evt.platform_ec = WSAGetLastError();

					if (0 == PostQueuedCompletionStatus(asio->iocp, 0, 0, (LPOVERLAPPED)o))
					{
						// If fails on posting an error completion event, throw a log and return error code.
						DWORD lastec = GetLastError();
						_log(context, JN_LL_ERROR, "jn_asio_query_job_completion: Failed on PostQueuedCompletionStatus() when reporting a sendto error. GetLastError(): %u", lastec);
					}
				}
			}
			break;

		default:
			break;
		}
		break;

	default:
		break;
	}

	overlapped->evt.userdata = host->userdata;

	cb(&overlapped->evt);
	context->free(overlapped);

	return JN_EC_OK;
}

JNEErrCodes
jn_asio_query_job_completion(JNAsio *asio, EventCallback cb, unsigned int interval_ms)
{
	if (asio->bShutdown)
		return JN_EC_SHUTTING_DOWN;

	return _query_tcpudp_job_completion(asio, cb, interval_ms);
}

JNEErrCodes
jn_asio_join_socket(JNAsio *asio, JNSocket socket)
{
	JNEErrCodes ec = JN_EC_OK;
	if (NULL == CreateIoCompletionPort((HANDLE)socket, asio->iocp, 0, 0))
	{
		DWORD le = GetLastError();
		_log(asio->context, JN_LL_ERROR, "jn_asio_join_socket: Failed on CreateIoCompletionPort(). GetLastError(): %u", le);
		ec = JN_EC_PLATFORM_ERROR;
	}
	return ec;
}


JNEErrCodes
jn_asio_cancel_socket_io(JNAsio *asio, JNSocket socket)
{
	if (0 == CancelIoEx((HANDLE)socket, NULL)) // Cancel all async I/O happening on this socket.
	{
		if (ERROR_NOT_FOUND != GetLastError()) // Spare the case of 'nothing to cancel' to be reported as an error.
			return JN_EC_PLATFORM_ERROR;
	}
	return JN_EC_OK;
}

JNAsioPeerData*
jn_asio_peerdata_create(const JNContext *ctx)
{
	JNAsioPeerData *pad = (JNAsioPeerData*)ctx->malloc(sizeof(JNAsioPeerData));
	pad->rdata = pad->wdata = NULL;
	pad->mutex = jn_mutex_create(ctx);
	return pad;
}

JNEErrCodes
jn_asio_peerdata_destroy(JNAsioPeerData* pad)
{
	const JNContext *ctx = pad->mutex->context;
	jn_mutex_destroy(pad->mutex);
	ctx->free(pad);
	return JN_EC_OK;
}

#endif // JN_PLATFORM_WINDOWS
