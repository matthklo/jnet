#pragma once

#include "platform.h"
#include <jnet/context.h>
#include <jnet/types.h>
#include "mutex.h"

typedef struct _JNFifo JNFifo;

typedef struct _JNAsioPeerData
{
	void*       rdata;
	void*       wdata;
	JNMutex*    mutex;
} JNAsioPeerData;

//========--------- Windows ---------========//
#if defined(JN_PLATFORM_WINDOWS)
#include <WS2tcpip.h>
#include <WinSock2.h>
#include <Mswsock.h>
#include <Windows.h>

typedef struct _JNAsio
{
	const JNContext*    context;
	HANDLE              iocp;
	LPFN_ACCEPTEX       pAcceptEx;
	LPFN_CONNECTEX      pConnectEx;
	JNFifo*             udp_sends;
	unsigned char       bShutdown;
} JNAsio;

//========--------- BSD/iOS/Mac ---------========//
#elif defined(JN_PLATFORM_BSD)
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

typedef struct _JNAsio
{
	const JNContext*    context;
	int                 kqueue_fd;
	JNFifo*             fifo_readypeers;
	JNFifo*             fifo_completions;
	JNFifo*             udp_sends;
	unsigned char       bShutdown;
} JNAsio;

#define INVALID_SOCKET (~0)
#define closesocket close

//========--------- Linux/Android ---------========//
#elif defined(JN_PLATFORM_LINUX)
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

typedef struct _JNAsio
{
	const JNContext*    context;
	int                 epoll_fd;
	JNFifo*             fifo_readypeers;
	JNFifo*             fifo_completions;
	JNFifo*             udp_sends;
	unsigned char       bShutdown;
} JNAsio;

#define INVALID_SOCKET (~0)
#define closesocket close

#else

#  error "Unsupported platform"

#endif

JNAsio*
jn_asio_create(const JNContext *context);

JNEErrCodes
jn_asio_shutdown(JNAsio *asio);

// Always return JN_EC_OK
JNEErrCodes
jn_asio_destroy(JNAsio *asio);

JNEErrCodes
jn_asio_deliver_job(JNAsio *asio, JNHostEvent *in_evt);

// Return values:
//    JN_EC_OK            : If a completion event has been dequeued.
//    JN_EC_NO_MATCH      : If timeout and no completion event has been dequeued.
//    JN_EC_PLATFORM_ERROR: The asio is terminating due to either destroying or error.
JNEErrCodes
jn_asio_query_job_completion(JNAsio *asio, EventCallback cb, unsigned int interval_ms);

JNEErrCodes
jn_asio_join_socket(JNAsio *asio, JNSocket socket);

JNEErrCodes
jn_asio_cancel_socket_io(JNAsio *asio, JNSocket socket);

JNAsioPeerData*
jn_asio_peerdata_create(const JNContext *ctx);

JNEErrCodes
jn_asio_peerdata_destroy(JNAsioPeerData* pad);
