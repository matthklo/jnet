#include "platform.h"
#include "fifo.h"

JNFifo*
jn_fifo_create(const JNContext *context)
{
	if (!context)
		return NULL;

	JNFifo* fifo = (JNFifo*)context->malloc(sizeof(JNFifo));
	if (fifo)
	{
		fifo->context = context;
		fifo->mutex = jn_mutex_create(context);
		if (!fifo->mutex)
		{
			context->free(fifo);
			return NULL;
		}
		fifo->head = fifo->end = NULL;
		fifo->size = 0;
	}
	return fifo;
}

void
jn_fifo_destroy(JNFifo *fifo, custom_free_func free_func)
{
	jn_mutex_lock(fifo->mutex);

	JNFifoRecord *r = fifo->head;
	while (r)
	{
		JNFifoRecord *p = r;
		r = r->next;
		if (free_func)
			free_func(p->data);
		fifo->context->free(p);
	}
	fifo->head = fifo->end = NULL;
	fifo->size = 0;

	jn_mutex_unlock(fifo->mutex);
	jn_mutex_destroy(fifo->mutex);
	fifo->mutex = NULL;

	fifo->context->free(fifo);
}

// Pop and return the first element of fifo. NULL if empty.
void*
jn_fifo_popfront(JNFifo *fifo)
{
	void *ret = NULL;
	jn_mutex_lock(fifo->mutex);

	if (fifo->head)
	{
		JNFifoRecord *r = fifo->head;
		fifo->head = r->next;
		if (fifo->head)
			fifo->head->prev = NULL;
		if (fifo->end == r)
			fifo->end = NULL;
		ret = r->data;
		fifo->size--;

		fifo->context->free(r);
	}

	jn_mutex_unlock(fifo->mutex);
	return ret;
}

// Return the size of fifo after push
unsigned int
jn_fifo_pushback(JNFifo *fifo, void *data)
{
	unsigned int ret = 0;
	jn_mutex_lock(fifo->mutex);

	JNFifoRecord *r = (JNFifoRecord*)fifo->context->malloc(sizeof(JNFifoRecord));
	r->data = data;
	r->next = NULL;

	if (fifo->end)
	{
		r->prev = fifo->end;
		fifo->end->next = r;
		fifo->end = r;
	}
	else
	{
		r->prev = NULL;
		fifo->head = fifo->end = r;
	}
	ret = ++(fifo->size);

	jn_mutex_unlock(fifo->mutex);
	return ret;
}

// Iterate through all records in the fifo, passing each record->data to the
// predictor function 'pred'. And remove those records whose return value of 
// predictor function is 1.
// Return the number of records has been removed.
// Note: O(N)
int
jn_fifo_remove(JNFifo *fifo, predictor_func pred, void *pred_param)
{
	int ret = 0;
	jn_mutex_lock(fifo->mutex);

	JNFifoRecord *r = fifo->head;
	while(r)
	{
		if (pred(r->data, pred_param))
		{
			JNFifoRecord *p = r;
			r = r->next;
			if (p->prev)
				p->prev->next = p->next;
			else
				fifo->head = p->next;
			if (p->next)
				p->next->prev = p->prev;
			else
				fifo->end = p->prev;
			fifo->context->free(p);
			ret++;
			fifo->size--;
			continue;
		}
		r = r->next;
	}

	jn_mutex_unlock(fifo->mutex);
	return ret;
}

void*
jn_fifo_front(JNFifo *fifo)
{
	void *ret = NULL;
	jn_mutex_lock(fifo->mutex);

	if (fifo->head)
		ret = fifo->head->data;

	jn_mutex_unlock(fifo->mutex);
	return ret;
}

void*
jn_fifo_back(JNFifo *fifo)
{
	void *ret = NULL;
	jn_mutex_lock(fifo->mutex);

	if (fifo->end)
		ret = fifo->end->data;

	jn_mutex_unlock(fifo->mutex);
	return ret;
}
