#include "platform.h"

/* kqueue is only available on BSD-like platform (BSD, iOS, MacOSX) */
#ifdef JN_PLATFORM_BSD
#include "asio.h"
#include <jnet/addr.h>
#include <jnet/peer.h>
#include "fifo.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/event.h>
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>

#define MAX_EVENTS_AT_ONCE (128)

static void _log(const JNContext *ctx, JNELogLevel level, const char *fmt, ...)
{
	va_list argptr;
	va_start(argptr, fmt);

	char buf[1024];
	vsprintf(buf, fmt, argptr);
	ctx->logger(level, buf);

	va_end(argptr);
}

JNAsio*
jn_asio_create(const JNContext *context)
{
	JNAsio *asio = (JNAsio*)context->malloc(sizeof(JNAsio));
	do
	{
		if (!asio)
			break;

		memset(asio, 0, sizeof(JNAsio));
		asio->context = context;
		
		asio->kqueue_fd = kqueue();
		if (-1 == asio->kqueue_fd)
		{
			_log(context, JN_LL_ERROR, "jn_asio_create: Failed on kqueue(). errno: %d", errno);
			context->free(asio);
			asio = NULL;
			break;
		}

		asio->fifo_readypeers = jn_fifo_create(context); // A fifo of JNPeers.
		asio->fifo_completions = jn_fifo_create(context); // A fifo of JNHostEvents.
		asio->udp_sends = jn_fifo_create(context); // A fifo of JNHostEvents (Outgoing event queue). Used by UDP

		if (!asio->fifo_readypeers || !asio->fifo_completions ||
				!asio->udp_sends)
		{
			_log(context, JN_LL_ERROR, "jn_asio_create: Failed on initializing internal data.");
			if (asio->fifo_readypeers)
				jn_fifo_destroy(asio->fifo_readypeers, context->free);
			if (asio->fifo_completions)
				jn_fifo_destroy(asio->fifo_completions, context->free);
			if (asio->udp_sends)
				jn_fifo_destroy(asio->udp_sends, context->free);
			context->free(asio);
			asio = NULL;
			break;
		}
	} while (0);
	return asio;
}

JNEErrCodes
jn_asio_shutdown(JNAsio *asio)
{
	asio->bShutdown = 1;
	return JN_EC_OK;
}

// Always return JN_EC_OK
JNEErrCodes
jn_asio_destroy(JNAsio *asio)
{
	close(asio->kqueue_fd);
	jn_fifo_destroy(asio->fifo_readypeers, asio->context->free);
	jn_fifo_destroy(asio->fifo_completions, asio->context->free);
	jn_fifo_destroy(asio->udp_sends, asio->context->free);
	asio->context->free(asio);
	return JN_EC_OK;
}

static JNEErrCodes
_asio_deliver_job_tcp(JNAsio *asio, JNEHostType htype, JNHostEvent *in_evt)
{
	// Prepare the event data
	JNHostEvent *e = (JNHostEvent*)asio->context->malloc(sizeof(JNHostEvent));
	if (!e)
		return JN_EC_OUT_OF_MEMORY;
	memcpy(e, in_evt, sizeof(JNHostEvent));
	e->ec = JN_EC_IN_PROGRESS;
	e->platform_ec = 0;

	JNEErrCodes ec = JN_EC_OK;
	jn_mutex_lock(e->peer->asio_data->mutex);
	switch (e->type)
	{
	case JN_EVENT_DISCONNECT:
		// Handle disconnect right here, post a completion event.
		// Host type check. Disconnect should be only available for TCP hosts.
		switch (htype)
		{
		case JN_HOST_TCP_SERVER:
			if (e->peer == e->peer->host->hostpeer)
			{
				asio->context->free(e);
				ec = JN_EC_INVALID_PARAMS;
				break;
			}
			// fall through
		case JN_HOST_TCP_CLIENT:
			jn_asio_cancel_socket_io(asio, e->peer->socket);
			e->peer->state = JN_PEER_STATE_DISCONNECTING;
			jn_fifo_pushback(asio->fifo_completions, e);
			jn_mutex_unlock(e->peer->asio_data->mutex);
			return JN_EC_OK;

		default:
			asio->context->free(e);
			ec = JN_EC_INVALID_PARAMS;
			break;
		}
		break;

	case JN_EVENT_ACCEPT:
	case JN_EVENT_RECV:
		if (e->peer->asio_data->rdata)
		{
			asio->context->free(e);
			ec = JN_EC_IN_PROGRESS; // another recv/accept asio on the same peer is in progress.
			break;
		}
		e->peer->asio_data->rdata = e;
		break;

	default: // SEND, CONNECT
		if (e->peer->asio_data->wdata)
		{
			asio->context->free(e);
			ec = JN_EC_IN_PROGRESS; // another write/connect asio on the same peer is in progress.
			break;
		}
		e->peer->asio_data->wdata = e;
		break;
	}
	jn_mutex_unlock(e->peer->asio_data->mutex);

	if (ec == JN_EC_OK)
	{
		// Assume the peer (socket) is ready for performing operation.
		jn_fifo_pushback(asio->fifo_readypeers, e->peer);
	}
	return ec;
}

static JNEErrCodes
_asio_deliver_job_udp(JNAsio *asio, JNEHostType htype, JNHostEvent *in_evt)
{
	// Prepare the event data
	JNHostEvent *e = (JNHostEvent*)asio->context->malloc(sizeof(JNHostEvent));
	if (!e)
		return JN_EC_OUT_OF_MEMORY;
	memcpy(e, in_evt, sizeof(JNHostEvent));
	e->ec = JN_EC_IN_PROGRESS;
	e->platform_ec = 0;

	switch (e->type)
	{
	case JN_EVENT_RECV:
		if (e->peer->asio_data->rdata)
		{
			asio->context->free(e);
			return JN_EC_IN_PROGRESS; // another recv/accept asio on the same peer is in progress.
		}
		e->peer->asio_data->rdata = e;
		break;

	case JN_EVENT_SEND:
		// maintain an internal outgoing fifo for UDP hosts
		jn_mutex_lock(e->peer->asio_data->mutex);
		if (1 == jn_fifo_pushback(asio->udp_sends, e)) // this packet is the only outgoing packet in progress.
		{
			jn_fifo_pushback(asio->fifo_readypeers, e->peer);
			e->peer->asio_data->wdata = e;
		}
		jn_mutex_unlock(e->peer->asio_data->mutex);
		break;

	default:
		asio->context->free(e);
		return JN_EC_INVALID_PARAMS;
	}

	// Assume the peer (socket) is ready for performing operation.
	jn_fifo_pushback(asio->fifo_readypeers, e->peer);
	return JN_EC_OK;
}

JNEErrCodes
jn_asio_deliver_job(JNAsio *asio, JNHostEvent *in_evt)
{
	if (asio->bShutdown)
		return JN_EC_SHUTTING_DOWN;

	JNEHostType htype = in_evt->peer->host->type;
	switch (htype)
	{
	case JN_HOST_TCP_SERVER:
	case JN_HOST_TCP_CLIENT:
		return _asio_deliver_job_tcp(asio, htype, in_evt);
	case JN_HOST_UDP_SERVER:
	case JN_HOST_UDP_CLIENT:
		return _asio_deliver_job_udp(asio, htype, in_evt);
	default:
		break;
	}
	return JN_EC_INVALID_PARAMS;
}

// Perform the requested operation. If will block (EAGAIN or EWOULDBLOCK), return 0.
// Otherwise post a completion event and return 1.
static int
_process(JNAsio *asio, JNHostEvent *evt)
{
	int ret = 0;
	JNPeer *peer = evt->peer;
	JNHost *host = peer->host;
	const JNContext *context = asio->context;

	switch (host->type)
	{
	case JN_HOST_TCP_CLIENT:
	case JN_HOST_TCP_SERVER:
		switch (evt->type)
		{
		case JN_EVENT_ACCEPT:
			{
				int invoke_accept = 0;
				int accepted_socket = accept(peer->socket, NULL, NULL);
				if (accepted_socket != -1)
				{
					// Create JNPeer for the newly accepted connection.
					JNPeer *p = (JNPeer*)context->malloc(sizeof(JNPeer));
					memset(p, 0, sizeof(JNPeer));
					p->host = host;
					p->socket = accepted_socket;
					p->state = JN_PEER_STATE_CONNECTED;
					p->localaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
					p->remoteaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
					p->asio_data = jn_asio_peerdata_create(context);
					getsockname(p->socket, (struct sockaddr*)p->localaddr.sockaddr,  (socklen_t*)&p->localaddr.socklen);
					getpeername(p->socket, (struct sockaddr*)p->remoteaddr.sockaddr, (socklen_t*)&p->remoteaddr.socklen);
					jn_address_load_port(&p->localaddr);
					jn_address_load_port(&p->remoteaddr);
					memcpy(&evt->addr, &p->remoteaddr, sizeof(JNAddress));
					evt->peer = p;
					evt->ec = JN_EC_OK;

					// join asio
					jn_asio_join_socket(asio, p->socket);
					invoke_accept = 1;
					ret = 1;
				}
				else if (errno != EWOULDBLOCK && errno != EAGAIN)
				{
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				
				if (ret)
				{
					peer->asio_data->rdata = NULL;
					jn_fifo_pushback(asio->fifo_completions, evt);
				}
				
				// Start next async accept
				if (invoke_accept && host->hostpeer->state == JN_PEER_STATE_LISTENING)
				{
					JNHostEvent evt;
					evt.type = JN_EVENT_ACCEPT;
					evt.peer = host->hostpeer;
					jn_asio_deliver_job(asio, &evt);
				}
			}
			break;
		
		case JN_EVENT_CONNECT:
			if (evt->data_len == 0) // not yet called connect()
			{
				int ec = connect(peer->socket, (const struct sockaddr*)evt->addr.sockaddr, (socklen_t)evt->addr.socklen);
				if (ec == 0)
				{
					evt->ec = JN_EC_OK;
					peer->state = JN_PEER_STATE_CONNECTED;
					ret = 1;
				}
				else if (errno != EINPROGRESS)
				{
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				else
				{
					peer->state = JN_PEER_STATE_CONNECTING;
					evt->data_len = 1; // mark a connect() call in progress.
				}
			}
			else // a connect() was called and waiting for result.
			{
				int val = 0;
				socklen_t valsize = sizeof(int);
				int ec = getsockopt(peer->socket, SOL_SOCKET, SO_ERROR, &val, &valsize);
				if (ec == 0 && val == 0)
				{
					evt->ec = JN_EC_OK;
					evt->data_len = 0;
					peer->state = JN_PEER_STATE_CONNECTED;
				}
				else
				{
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = val;
				}
				ret = 1;
			}
			
			if (ret)
			{
				peer->remoteaddr.socklen = JN_SOCKADDR_MAX_LENGTH;
				getpeername(peer->socket, (struct sockaddr *)peer->remoteaddr.sockaddr, (socklen_t*)&peer->remoteaddr.socklen);
				peer->asio_data->wdata = NULL;
				jn_fifo_pushback(asio->fifo_completions, evt);
			}
			break;

		case JN_EVENT_RECV:
			{
				ssize_t bytes = recv(peer->socket, evt->in_data, (size_t)evt->data_len, MSG_DONTWAIT);
				if (bytes >= 0)
				{
					evt->data_len = (unsigned int)bytes;
					evt->ec = JN_EC_OK;
					ret = 1;
				}
				else if (errno != EWOULDBLOCK && errno != EAGAIN)
				{
					evt->data_len = 0;
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				
				if (ret)
				{
					peer->asio_data->rdata = NULL;
					jn_fifo_pushback(asio->fifo_completions, evt);
				}
			}
			break;

		case JN_EVENT_SEND:
			{
				ssize_t bytes = send(peer->socket, evt->out_data, (size_t)evt->data_len, MSG_DONTWAIT);
				if (bytes >=0 )
				{
					evt->data_len = (unsigned int)bytes;
					evt->ec = JN_EC_OK;
					ret = 1;
				}
				else if (errno != EWOULDBLOCK && errno != EAGAIN)
				{
					evt->data_len = 0;
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				
				if (ret)
				{
					peer->asio_data->wdata = NULL;
					jn_fifo_pushback(asio->fifo_completions, evt);
				}
			}
			break;

		default:
			ret = 1;
			break;
		}
		break;
		
	case JN_HOST_UDP_CLIENT:
	case JN_HOST_UDP_SERVER:
		switch (evt->type)
		{
		case JN_EVENT_RECV:
			{
				evt->addr.socklen = JN_SOCKADDR_MAX_LENGTH;
				ssize_t bytes = recvfrom(peer->socket, evt->in_data, (size_t)evt->data_len, MSG_DONTWAIT, 
						(struct sockaddr*)evt->addr.sockaddr, (socklen_t*)&evt->addr.socklen);
				if (bytes >= 0)
				{
					evt->data_len = (unsigned int)bytes;
					if (bytes > 0)
						jn_address_load_port(&evt->addr);
					evt->ec = JN_EC_OK;
					ret = 1;
				}
				else if (errno != EWOULDBLOCK && errno != EAGAIN)
				{
					evt->data_len = 0;
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				
				if (ret)
				{
					peer->asio_data->rdata = NULL;
					jn_fifo_pushback(asio->fifo_completions, evt);
				}
			}
			break;

		case JN_EVENT_SEND:
			{
				ssize_t bytes = sendto(peer->socket, evt->out_data, (size_t)evt->data_len, MSG_DONTWAIT,
						(const struct sockaddr*)evt->addr.sockaddr, (socklen_t)evt->addr.socklen);
				if (bytes >= 0 )
				{
					evt->data_len = (unsigned int)bytes;
					evt->ec = JN_EC_OK;
					ret = 1;
				}
				else if (errno != EWOULDBLOCK && errno != EAGAIN)
				{
					evt->data_len = 0;
					evt->ec = JN_EC_PLATFORM_ERROR;
					evt->platform_ec = errno;
					ret = 1;
				}
				
				if (ret)
				{
					peer->asio_data->wdata = NULL;
					jn_fifo_pushback(asio->fifo_completions, evt);
				}
			}
			break;

		default:
			ret = 1;
			break;
		}
		break;
	
	default:
		ret = 1;
		break;
	}
	
	return ret;
}

static JNEErrCodes
_query_tcpudp_job_completion(JNAsio *asio, EventCallback cb, unsigned int interval_ms)
{
	int quick_poll = 0;
	JNHost *host = NULL;
	const JNContext *context = asio->context;

	// Process completion events
	JNHostEvent *e = jn_fifo_popfront(asio->fifo_completions);
	if (e)
	{
		quick_poll = 1;

		switch (e->type)
		{
		case JN_EVENT_DISCONNECT:
			e->ec = JN_EC_OK;
			switch (e->peer->host->type)
			{
			case JN_HOST_TCP_SERVER:
			case JN_HOST_TCP_CLIENT:
				e->peer->state = JN_PEER_STATE_DISCONNECTED;
				closesocket(e->peer->socket);
				e->peer->socket = INVALID_SOCKET;
				break;

			default:
				break;
			} // end of switch (e->peer->host->type)
			break;

		case JN_EVENT_SEND:
			e->ec = JN_EC_OK;
			switch (e->peer->host->type)
			{
			case JN_HOST_UDP_SERVER:
			case JN_HOST_UDP_CLIENT:
				{
					//// Enter critical section
					jn_mutex_lock(e->peer->asio_data->mutex);

					JNHostEvent *o = (JNHostEvent *)jn_fifo_popfront(asio->udp_sends);
					if (o != e)
					{
						// Fatal error.
						_log(context, JN_LL_ERROR, "jn_asio_query_job_completion: FATAL ERROR: outgoing event is not at the front of udp_sends.");
					}

					// If there are more outgoing events, invoke the next one.
					o = jn_fifo_front(asio->udp_sends);
					if (o)
					{
						jn_fifo_pushback(asio->fifo_readypeers, o->peer);
						e->peer->asio_data->wdata = o;
					}

					jn_mutex_unlock(e->peer->asio_data->mutex);
					//// Leave critical section
				}
				break;

			default:
				break;
			} // end of switch (e->peer->host->type)
			break;

		default:
			break;
		} // end of switch (e->type)

		e->userdata = e->peer->host->userdata;
		cb(e);

		asio->context->free(e);
	} // end of if (e)

	// Perform operation on ready sockets
	JNPeer *p = jn_fifo_popfront(asio->fifo_readypeers);
	if (p)
	{
		struct kevent changes[2] = { 0 };
		int change_cnt = 0;

		jn_mutex_lock(p->asio_data->mutex);
		if (p->asio_data->rdata)
		{
			quick_poll = 1;
			if (0 == _process(asio, (JNHostEvent*)p->asio_data->rdata))
			{
				changes[change_cnt].filter = EVFILT_READ;
				changes[change_cnt].flags = EV_ADD | EV_ONESHOT | EV_ENABLE;
				changes[change_cnt].ident = p->socket;
				changes[change_cnt].udata = (void*)p;
				change_cnt++;
			}
		}

		if (p->asio_data->wdata)
		{
			quick_poll = 1;
			if (0 == _process(asio, (JNHostEvent*)p->asio_data->wdata))
			{
				changes[change_cnt].filter = EVFILT_WRITE;
				changes[change_cnt].flags = EV_ADD | EV_ONESHOT | EV_ENABLE;
				changes[change_cnt].ident = p->socket;
				changes[change_cnt].udata = (void*)p;
				change_cnt++;
			}
		}

		if (change_cnt > 0) // rearm
		{
			kevent(asio->kqueue_fd, changes, change_cnt, 0, 0, 0);
		}
		jn_mutex_unlock(p->asio_data->mutex);
	}

	// Do epoll_wait() for more ready sockets
	struct timespec ts;
	ts.tv_sec = 0;
	ts.tv_nsec = (quick_poll) ? 0 : interval_ms * 1000000;

	struct kevent evts[MAX_EVENTS_AT_ONCE];
	int nevts = kevent(asio->kqueue_fd, 0, 0, evts, MAX_EVENTS_AT_ONCE, &ts);
	if (nevts < 0)
	{
		if (errno == EINTR) // interrupted by signal, which is fine.
			return JN_EC_NO_MATCH;

		_log(asio->context, JN_LL_ERROR, "jn_asio_query_job_completion: Failed on kevent(). errno:%d.", errno);
		return JN_EC_PLATFORM_ERROR;
	}
	else
	{
		int i;
		for (i = 0; i<nevts; ++i)
		{
			short   filter = evts[i].filter;
			u_short flags = evts[i].flags;
			JNPeer *peer = (JNPeer*)evts[i].udata;

			jn_mutex_lock(peer->asio_data->mutex);
			switch (filter)
			{
			case EVFILT_READ:
				if (flags & EV_EOF)
				{
					if (peer->asio_data->rdata)
					{
						JNHostEvent *e = (JNHostEvent*)peer->asio_data->rdata;
						e->ec = JN_EC_PLATFORM_ERROR;
						e->platform_ec = 0;
						e->data_len = 0;
						jn_fifo_pushback(asio->fifo_completions, e);
						peer->asio_data->rdata = NULL;
					}
				}
				else
				{
					jn_fifo_pushback(asio->fifo_readypeers, peer);
				}
				break;

			case EVFILT_WRITE:
				if (flags & EV_EOF)
				{
					if (peer->asio_data->wdata)
					{
						JNHostEvent *e = (JNHostEvent*)peer->asio_data->wdata;
						e->ec = JN_EC_PLATFORM_ERROR;
						e->platform_ec = 0;
						e->data_len = 0;
						jn_fifo_pushback(asio->fifo_completions, e);
						peer->asio_data->wdata = NULL;
					}
				}
				else
				{
					jn_fifo_pushback(asio->fifo_readypeers, peer);
				}
				break;

			default:
				break;
			}
			jn_mutex_unlock(peer->asio_data->mutex);
		}
	}

	return (nevts == 0) ? JN_EC_NO_MATCH : JN_EC_OK;
}

JNEErrCodes
jn_asio_query_job_completion(JNAsio *asio, EventCallback cb, unsigned int interval_ms)
{
	if (asio->bShutdown)
		return JN_EC_SHUTTING_DOWN;

	return _query_tcpudp_job_completion(asio, cb, interval_ms);
}

JNEErrCodes
jn_asio_join_socket(JNAsio *asio, JNSocket socket)
{
	// request the socket to be non-blocking
	int flags = fcntl(socket, F_GETFL);
	if (-1 == fcntl(socket, F_SETFL, flags | O_NONBLOCK))
	{
		_log(asio->context, JN_LL_ERROR, "jn_asio_join_socket: Failed on fcntl(). errno: %d.", errno);
		return JN_EC_PLATFORM_ERROR;
	}
	return JN_EC_OK;
}

static int
_pred_readypeers(void *data, void *pred_param)
{
	JNPeer *peer = (JNPeer*)data;
	JNSocket socket = *((JNSocket*)pred_param);
	
	return (!peer || peer->socket == socket) ? 1 : 0;
}

static int
_pred_completions(void *data, void *pred_param)
{
	JNHostEvent *evt = (JNHostEvent*)data;
	JNSocket socket = *((JNSocket*)pred_param);
	
	if (!evt || evt->peer->socket == socket)
	{
		if (evt)
			evt->peer->host->context.free(evt);
		return 1;
	}
	return 0;
}

JNEErrCodes
jn_asio_cancel_socket_io(JNAsio *asio, JNSocket socket)
{
	// Ensure the socket has been removed from kqueue monitoring list.
	struct kevent changes[2] = {0};
	changes[0].ident = changes[1].ident = socket;
	changes[0].flags = changes[1].flags = EV_DELETE;
	changes[0].filter = EVFILT_READ;
	changes[1].filter = EVFILT_WRITE;
	int ec = kevent(asio->kqueue_fd, changes, 2, 0, 0, 0);

	// remove records in fifo_readypeers.
	jn_fifo_remove(asio->fifo_readypeers, _pred_readypeers, &socket);

	// remove records in fifo_completions.
	jn_fifo_remove(asio->fifo_completions, _pred_completions, &socket);

	return JN_EC_OK;
}

JNAsioPeerData*
jn_asio_peerdata_create(const JNContext *ctx)
{
	JNAsioPeerData *pad = (JNAsioPeerData*)ctx->malloc(sizeof(JNAsioPeerData));
	pad->rdata = pad->wdata = NULL;
	pad->mutex = jn_mutex_create(ctx);
	return pad;
}

JNEErrCodes
jn_asio_peerdata_destroy(JNAsioPeerData* pad)
{
	const JNContext *ctx = pad->mutex->context;
	jn_mutex_destroy(pad->mutex);
	ctx->free(pad);
	return JN_EC_OK;
}
#endif // JN_PLATFORM_BSD
