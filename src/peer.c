#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include "platform.h"
#include <jnet/peer.h>
#include "asio.h"
#include <string.h>

JNEErrCodes
jn_peer_send(JNPeer *peer, size_t len, const char *buf, 
        const JNAddress *udp_dest)
{
	JNHost *host;
	JNEErrCodes ret = JN_EC_INVALID_PARAMS;
	JNHostEvent evt = { 0 };

	if (!peer || !peer->host || !buf || !len)
		return JN_EC_INVALID_PARAMS;
	if ((peer->state != JN_PEER_STATE_CONNECTED) && (peer->state != JN_PEER_UNISTATE))
		return JN_EC_INVALID_STATE;
	host = peer->host;

	evt.type = JN_EVENT_SEND;
	evt.out_data = buf;
	evt.data_len = (unsigned int)len;
	evt.peer = peer;

	switch (host->type)
	{
	case JN_HOST_TCP_CLIENT:
	case JN_HOST_TCP_SERVER:
		ret = jn_asio_deliver_job((JNAsio*)host->asio_opaque, &evt);
		break;

	case JN_HOST_UDP_CLIENT:
	case JN_HOST_UDP_SERVER:
		// TODO: outgoing queue for udp send
		memcpy(&evt.addr, udp_dest, sizeof(JNAddress));
		ret = jn_asio_deliver_job((JNAsio*)host->asio_opaque, &evt);
		break;

	default:
		ret = JN_EC_INVALID_PARAMS;
		break;
	}
	return ret;
}

JNEErrCodes
jn_peer_recv(JNPeer *peer, size_t len, char *buf)
{
	JNHost *host;
	JNEErrCodes ret = JN_EC_INVALID_PARAMS;
	JNHostEvent evt = { 0 };

	if (!peer || !peer->host || !buf || !len)
		return JN_EC_INVALID_PARAMS;
	if ((peer->state != JN_PEER_STATE_CONNECTED) && (peer->state != JN_PEER_UNISTATE))
		return JN_EC_INVALID_STATE;
	host = peer->host;

	evt.type = JN_EVENT_RECV;
	evt.in_data = buf;
	evt.data_len = (unsigned int)len;
	evt.peer = peer;

	switch (host->type)
	{
	case JN_HOST_TCP_CLIENT:
	case JN_HOST_TCP_SERVER:
		ret = jn_asio_deliver_job((JNAsio*)host->asio_opaque, &evt);
		break;

	case JN_HOST_UDP_CLIENT:
	case JN_HOST_UDP_SERVER:
		evt.addr.socklen = JN_SOCKADDR_MAX_LENGTH;
		ret = jn_asio_deliver_job((JNAsio*)host->asio_opaque, &evt);
		break;

	default:
		ret = JN_EC_INVALID_PARAMS;
		break;
	}
	return ret;
}

// Note: only meaningful for TCP peers
JNEErrCodes
jn_peer_shutdown(JNPeer *peer, JNEShutdownDir dir)
{
	int ec;
	int how;
#ifdef JN_PLATFORM_WINDOWS
	if (dir == JN_SHUTDOWN_RD)
		how = SD_RECEIVE;
	else if (dir == JN_SHUTDOWN_WR)
		how = SD_SEND;
	else
		how = SD_BOTH;
#else
	if (dir == JN_SHUTDOWN_RD)
		how = SHUT_RD;
	else if (dir == JN_SHUTDOWN_WR)
		how = SHUT_WR;
	else
		how = SHUT_RDWR;
#endif
	ec = shutdown(peer->socket, how);
	return (ec == 0)? JN_EC_OK : JN_EC_PLATFORM_ERROR;
}

JNEErrCodes
jn_peer_setsockopt(JNPeer *peer, int level, int optname, const void *optval, unsigned int optlen)
{
	int ec;
	ec = setsockopt(peer->socket, level, optname, optval, optlen);
	return (ec == 0) ? JN_EC_OK : JN_EC_PLATFORM_ERROR;
}

JNEErrCodes
jn_peer_disconnect(JNPeer *peer)
{
	JNHostEvent evt = { 0 };

	// host peers can not be disconnected.
	if (!peer || (peer == peer->host->hostpeer))
		return JN_EC_INVALID_PARAMS;

	evt.type = JN_EVENT_DISCONNECT;
	evt.peer = peer;
	peer->state = JN_PEER_STATE_DISCONNECTING;
	return jn_asio_deliver_job((JNAsio*)peer->host->asio_opaque, &evt);
}

// NOTE: Destroy the peer object.
JNEErrCodes
jn_peer_destroy(JNPeer *peer)
{
	// hostpeer can not be destroyed manually. destroy the host object instead.
	if (!peer || (peer == peer->host->hostpeer))
		return JN_EC_INVALID_PARAMS;

	peer->userdata = NULL;
	jn_peer_cancel_io(peer);
	jn_asio_peerdata_destroy(peer->asio_data);
	peer->host->context.free(peer);
	return JN_EC_OK;
}

JNEErrCodes
jn_peer_cancel_io(JNPeer *peer)
{
	JNEErrCodes ret;

	ret = jn_asio_cancel_socket_io((JNAsio*)peer->host->asio_opaque, peer->socket);
	if (JN_EC_OK == ret)
	{
		if (peer->asio_data->rdata)
		{
			peer->host->context.free(peer->asio_data->rdata);
			peer->asio_data->rdata = NULL;
		}
		if (peer->asio_data->wdata)
		{
			peer->host->context.free(peer->asio_data->wdata);
			peer->asio_data->wdata = NULL;
		}
	}
	return ret;
}

