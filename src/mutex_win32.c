#include "platform.h"
#include "mutex.h"

#ifdef JN_PLATFORM_WINDOWS
#include <Windows.h>

JNMutex*
jn_mutex_create(const JNContext *ctx)
{
	if (!ctx)
		return NULL;
	
	JNMutex *m = (JNMutex*)ctx->malloc(sizeof(JNMutex));
	if (m)
	{
		m->context = ctx;
		CRITICAL_SECTION *cs = (CRITICAL_SECTION*)ctx->malloc(sizeof(CRITICAL_SECTION));
		if (!cs)
		{
			ctx->free(m);
			return NULL;
		}
		InitializeCriticalSection(cs);
		m->opaque = cs;
	}
	return m;
}

int
jn_mutex_lock(JNMutex *mutex)
{
	EnterCriticalSection((CRITICAL_SECTION*)mutex->opaque);
	return 1;
}

int
jn_mutex_trylock(JNMutex *mutex)
{
	return (TRUE == TryEnterCriticalSection((CRITICAL_SECTION*)mutex->opaque)) ? 1 : 0;
}

int
jn_mutex_unlock(JNMutex *mutex)
{
	LeaveCriticalSection((CRITICAL_SECTION*)mutex->opaque);
	return 1;
}

int
jn_mutex_destroy(JNMutex *mutex)
{
	if (mutex)
	{
		if (mutex->opaque)
		{
			DeleteCriticalSection((CRITICAL_SECTION*)mutex->opaque);
			mutex->context->free(mutex->opaque);
		}
		mutex->context->free(mutex);
	}
	return 1;
}

#endif
