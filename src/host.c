#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#endif

#include "platform.h"
#include <jnet/addr.h>
#include <jnet/host.h>
#include <jnet/peer.h>
#include "asio.h"
#include <stdio.h>
#include <string.h>


JNHost*
jn_host_create(const JNContext *ctx, JNEHostType type, const JNAddress *addr, EventCallback cb, void *userdata, unsigned short flags)
{
	JNHost *h;
	JNAsio *asio;
	JNEIPProtocolVersion ipver;

	if (!ctx || type >= JN_HOST_INVALID || !cb)
		return NULL;

	h = (JNHost*)ctx->malloc(sizeof(JNHost));
	memset(h, 0, sizeof(JNHost));
	h->hostpeer = (JNPeer*)ctx->malloc(sizeof(JNPeer));
	memset(h->hostpeer, 0, sizeof(JNPeer));
	h->hostpeer->host = h;

	// If 'addr' is NULL, assume IPv4 is preferred. Unless 'JN_HOST_FLAG_IPV6_CLIENT' is specified in 'flags'.
	if (NULL == addr)
	{
		unsigned short addrflags = JN_ADDRESS_FLAG_PASSIVE;
		if (flags & JN_HOST_FLAG_IPV6_CLIENT)
			addrflags |= JN_ADDRESS_FLAG_IPV6;
		jn_address_set(&h->hostpeer->localaddr, NULL, 0, addrflags);
	}
	else
	{
		memcpy(&h->hostpeer->localaddr, addr, sizeof(JNAddress));
	}

	ipver = jn_address_tell_ipver(&h->hostpeer->localaddr);
	if (JN_IPPROTO_INVALID == ipver)
	{
		ctx->free(h->hostpeer);
		ctx->free(h);
		return NULL;
	}

	h->type = type;
	h->hostpeer->socket = INVALID_SOCKET;
	h->hostpeer->state = JN_PEER_STATE_DISCONNECTED;
	h->hostpeer->asio_data = jn_asio_peerdata_create(ctx);
	h->callback = cb;
	h->userdata = userdata;
	memcpy(&h->context, ctx, sizeof(JNContext));

	asio = jn_asio_create(ctx);
	h->asio_opaque = (void*)asio;

	switch (type)
	{
	case JN_HOST_TCP_CLIENT:
		h->hostpeer->socket = socket(ipver == JN_IPPROTO_IPV4 ? AF_INET : AF_INET6, SOCK_STREAM, IPPROTO_TCP);
		if (bind(h->hostpeer->socket, (const struct sockaddr*)h->hostpeer->localaddr.sockaddr, (int)h->hostpeer->localaddr.socklen))
		{
			ctx->logger(JN_LL_ERROR, "jn_host_create: Failed on binding TCP client socket.");
			closesocket(h->hostpeer->socket);
			h->hostpeer->socket = INVALID_SOCKET;
			break;
		}
		jn_asio_join_socket(asio, h->hostpeer->socket);
		h->hostpeer->state = JN_PEER_STATE_DISCONNECTED;
		break;

	case JN_HOST_TCP_SERVER:
		{
			JNHostEvent evt;

			h->hostpeer->socket = socket(ipver == JN_IPPROTO_IPV4 ? AF_INET : AF_INET6, SOCK_STREAM, IPPROTO_TCP);
			if (bind(h->hostpeer->socket, (const struct sockaddr*)h->hostpeer->localaddr.sockaddr, (int)h->hostpeer->localaddr.socklen))
			{
				ctx->logger(JN_LL_ERROR, "jn_host_create: Failed on binding TCP server socket.");
				closesocket(h->hostpeer->socket);
				h->hostpeer->socket = INVALID_SOCKET;
				break;
			}
			if (listen(h->hostpeer->socket, 128))
			{
				char buf[1024];
				sprintf(buf, "jn_host_create: Failed on listen TCP server socket.");
				ctx->logger(JN_LL_ERROR, buf);
				closesocket(h->hostpeer->socket);
				h->hostpeer->socket = INVALID_SOCKET;
				break;
			}
			jn_asio_join_socket(asio, h->hostpeer->socket);
			h->hostpeer->state = JN_PEER_STATE_LISTENING;

			// Start async accept
			evt.type = JN_EVENT_ACCEPT;
			evt.peer = h->hostpeer;
			if (JN_EC_OK != jn_asio_deliver_job(asio, &evt))
			{
				ctx->logger(JN_LL_ERROR, "jn_host_create: Failed on starting async accept on TCP server socket.");
				// do not destroy the host.
			}
		}
		break;

	case JN_HOST_UDP_CLIENT:
	case JN_HOST_UDP_SERVER:
		h->hostpeer->socket = socket(ipver == JN_IPPROTO_IPV4 ? AF_INET : AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
		if (bind(h->hostpeer->socket, (const struct sockaddr*)h->hostpeer->localaddr.sockaddr, (int)h->hostpeer->localaddr.socklen))
		{
			ctx->logger(JN_LL_ERROR, "jn_host_create: Failed on binding UDP server socket.");
			closesocket(h->hostpeer->socket);
			h->hostpeer->socket = INVALID_SOCKET;
			break;
		}
		jn_asio_join_socket(asio, h->hostpeer->socket);
		h->hostpeer->state = JN_PEER_UNISTATE;
		break;

	default:
		break;
	}

	if (h->hostpeer->socket == INVALID_SOCKET)
	{
		ctx->logger(JN_LL_ERROR, "jn_host_create: Failed on creating host socket.");
		ctx->free(h->hostpeer);
		ctx->free(h);
		h = NULL;
	}
	return h;
}

JNEErrCodes
jn_host_shutdown(JNHost *host)
{
	JNEErrCodes ret = JN_EC_INVALID_PARAMS;
	if (host && host->hostpeer)
	{
		// Try canceling all async I/O on happening.
		if (host->type == JN_HOST_TCP_SERVER)
			host->hostpeer->state = JN_PEER_STATE_DISCONNECTED; // change from LISTENING to DISCONNECTED;

		JNAsio *asio = (JNAsio*)host->asio_opaque;
		jn_asio_shutdown(asio);
		jn_peer_cancel_io(host->hostpeer);
		ret = JN_EC_OK;
	}
	return ret;
}

JNEErrCodes
jn_host_destroy(JNHost *host)
{
	JNEErrCodes ret = JN_EC_INVALID_PARAMS;
	if (host && host->hostpeer)
	{
		JNAsio *asio = (JNAsio*)host->asio_opaque;

		// NOTE: Destroying a TCP server host does not destroy all connectioned JNPeer.
		jn_asio_destroy(asio);

		closesocket(host->hostpeer->socket);
		jn_asio_peerdata_destroy(host->hostpeer->asio_data);
		host->context.free(host->hostpeer);
		host->context.free(host);
		ret = JN_EC_OK;
	}
	return ret;
}

JNEErrCodes
jn_host_connect(JNHost *host, const JNAddress *addr, unsigned short flags)
{
	JNHostEvent evt;
	JNEErrCodes ret;

	if (!host || !addr || addr->port == 0)
		return JN_EC_INVALID_PARAMS;
	
	if (host->type != JN_HOST_TCP_CLIENT)
		return JN_EC_WRONG_HOST_TYPE;

	evt.type = JN_EVENT_CONNECT;
	evt.peer = host->hostpeer;
	evt.data_len = 0;
	memcpy(&evt.addr, addr, sizeof(JNAddress));

	ret = jn_asio_deliver_job((JNAsio*)host->asio_opaque, &evt);
	return ret;
}

JNEErrCodes
jn_host_handle(JNHost *host, unsigned int interval_ms)
{
	return jn_asio_query_job_completion((JNAsio*)host->asio_opaque, host->callback, interval_ms);
}

