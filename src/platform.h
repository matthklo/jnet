#pragma once

#ifdef USE_VLD
#include <vld.h>
#endif

/*
* Detecting host platform via pre-defined compiler macros.
* http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
*/

// Cygwin-POSIX
#if !defined(JN_PLATFORM_SPECIFIED) && (defined(__CYGWIN__) || defined(__CYGWIN32__))
#  define JN_PLATFORM_CYGWIN 1
#  define JN_PLATFORM_SPECIFIED 1
#endif

// Windows-CE
#if !defined(JN_PLATFORM_SPECIFIED) && defined(_WIN32_WCE)
#  define JN_PLATFORM_WINDOWS 1
#  define JN_PLATFORM_WINCE 1
#  define JN_PLATFORM_SPECIFIED 1
#endif

// Windows-desktop
#if !defined(JN_PLATFORM_SPECIFIED) && defined(_WIN32)
#  define JN_PLATFORM_WINDOWS 1
#  if defined(__MINGW32__) || defined(__MINGW64__)
#    define JN_PLATFORM_MINGW
#  endif
#  define JN_PLATFORM_SPECIFIED 1
#endif

// x86-MaxOS and IOS
#if !defined(JN_PLATFORM_SPECIFIED) && defined(__APPLE__) && defined(__MACH__)
#  include <TargetConditionals.h>
#  if TARGET_IPHONE_SIMULATOR == 1
#    define JN_PLATFORM_IOSSIM 1
#  elif TARGET_OS_IPHONE == 1
#    define JN_PLATFORM_IOS 1
#  elif TARGET_OS_MAC == 1
#    define JN_PLATFORM_MAC 1
#  else
#    error This is an un-supported mac platform.
#  endif
#  define JN_PLATFORM_APPLE 1
#  define JN_PLATFORM_BSD 1
#  define JN_PLATFORM_SPECIFIED 1
#endif

// BSD-family
#if !defined(JN_PLATFORM_SPECIFIED) && (defined(__DragonFly__) || defined(__FreeBSD__) || defined(__NetBSD__) || defined(__OpenBSD__))
#  define JN_PLATFORM_BSD 1
#  if defined(__DragonFly__)
#    define JN_PLATFORM_DRAGONFLY 1
#  elif defined(__FreeBSD__)
#    define JN_PLATFORM_FREEBSD 1
#  elif defined(__NetBSD__)
#    define JN_PLATFORM_NETBSD 1
#  elif defined(__OpenBSD__)
#    define JN_PLATFORM_OPENBSD 1
#  endif
#  define JN_PLATFORM_SPECIFIED 1
#endif

// Linux/Android
#if !defined(JN_PLATFORM_SPECIFIED) && defined(__unix__) && defined(__linux__)
#  if defined(__ANDROID__)
#    define JN_PLATFORM_ANDROID 1
#  endif
#  define JN_PLATFORM_LINUX 1
#  define JN_PLATFORM_SPECIFIED 1
#endif

// Solaris
#if !defined(JN_PLATFORM_SPECIFIED) && defined(__sun) && defined(__SVR4)
#  define JN_PLATFORM_SOLARIS 1
#  define JN_PLATFORM_SPECIFIED 1
#endif

