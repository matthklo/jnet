#pragma once

#include "../jnet/context.h"

namespace jnet
{

int
Initialize()
{
	return ::jn_initialize();
}

void
Terminate()
{
	::jn_terminate();
}

class Context
{
	typedef void* (*UserMallocFunc) (size_t);
	typedef void* (*UserReallocFunc) (void*, size_t);
	typedef void  (*UserFreeFunc) (void*);
	typedef void  (*UserLoggerFunc) (JNELogLevel level, const char* msg);

public:
	Context()
	{
		pImpl = ::jn_context_create();
		bOwnContext = true;
	}

	explicit Context(JNContext *p)
	{
		pImpl = p;
		bOwnContext = false;
	}

	~Context()
	{
		destroy();
	}

#ifdef JN_CPP11
	Context(Context && other)
	{
		*this = other;
	}

	Context& operator=( Context && other )
	{
		destroy();
		pImpl = other.pImpl;
		other.pImpl = NULL;
		bOwnContext = other.bOwnContext;
		other.bOwnContext = false;
		return *this;
	}
#endif

	UserMallocFunc getUserMallocFunc() const { return pImpl->malloc; }
	UserReallocFunc getUserReallocFunc() const { return pImpl->realloc; }
	UserFreeFunc getUserFreeFunc() const { return pImpl->free; }
	UserLoggerFunc getUserLoggerFunc() const { return pImpl->logger; }

	void setUserMallocFunc(UserMallocFunc f) { pImpl->malloc = f; }
	void setUserReallocFunc(UserReallocFunc f) { pImpl->realloc = f; }
	void setUserFreeFunc(UserFreeFunc f) { pImpl->free = f; }
	void setUserLoggerFunc(UserLoggerFunc f) { pImpl->logger = f; }

	JNContext* getRawContext() const { return pImpl; }

private:
	// non-copyable
	Context( const Context &other ) { }
	Context& operator=( const Context & other ) { return *this; }
	
	void destroy()
	{
		if (bOwnContext)
			::jn_context_destroy(pImpl); /* Ok with the case pImpl is NULL */
		pImpl = NULL;
		bOwnContext = false;
	}

	bool bOwnContext;
	JNContext *pImpl;
};


} // end of namespace jnet
