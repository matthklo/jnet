#pragma once

#include "../jnet/host.h"
#include "context.hpp"
#include "addr.hpp"
#include "peer.hpp"

namespace jnet
{

typedef JNHostEvent HostEvent;

class IHostEventListener
{
public:
	virtual void onEvent(Peer *peer, HostEvent *evt) = 0;
};

class Host
{
public:
	virtual ~Host()
	{
		if (pHostPeer)
			delete pHostPeer;
		::jn_host_destroy(pImpl);
	}

#ifdef JN_CPP11
	// Host objects are non-movable
	Host ( Host && other ) = delete;
	Host& operator= ( Host && other ) = delete;
#endif

	JNEErrCodes
	shutdown()
	{
		return ::jn_host_shutdown(pImpl);
	}

	JNEErrCodes
	connect(const Address& addr, unsigned short flags = 0)
	{
		return ::jn_host_connect(pImpl, addr.getRawAddress(), flags);
	}

	JNEErrCodes
	handle(unsigned int internalMs)
	{
		return ::jn_host_handle(pImpl, internalMs);
	}

	EventCallback
	getEventCallbackFunc() const
	{
		return pImpl->callback;
	}

	void
	setEventCallbackFunc(EventCallback cb)
	{
		pImpl->callback = cb;
	}

	JNEHostType
	getType() const
	{
		return pImpl->type;
	}

	Peer*
	getHostPeer()
	{
		if (!pHostPeer)
			pHostPeer = new Peer(pImpl->hostpeer, false);
		return pHostPeer;
	}

protected:
	Host()
		: pImpl(NULL), pHostPeer(NULL) {}

	static void callbackDispatcher(JNHostEvent *evt)
	{
		Host *h = reinterpret_cast<Host*>(evt->userdata);
		Peer *peer = reinterpret_cast<Peer*>(evt->peer->userdata);
		if (NULL == peer)
		{
			switch (h->pImpl->type)
			{
			case JN_HOST_TCP_SERVER:
			case JN_HOST_TCP_CLIENT:
				peer = new Peer(evt->peer);
				break;
			default:
				peer = h->getHostPeer();
				break;
			}
			evt->peer->userdata = peer;
		}
		h->pListener->onEvent(peer, evt);
	}

	JNHost*             pImpl;
	Peer*               pHostPeer;
	IHostEventListener* pListener;

private:
	// Host objects are non-copyable.
	Host(const Host & other) {}
	Host& operator= (const Host & other) { return *this; }
};

class TcpServerHost : public Host
{
public:
	TcpServerHost(const Context& context, const Address& addr, IHostEventListener *listener, unsigned short flags = JN_HOST_FLAG_NONE)
	{
		pListener = listener;
		pImpl = ::jn_host_create(context.getRawContext(), JN_HOST_TCP_SERVER, addr.getRawAddress(), callbackDispatcher, this, flags);
	}

	// non-copyable
private:
	TcpServerHost(const TcpServerHost& other) {}
	TcpServerHost& operator=(const TcpServerHost& other) { return *this; }
};

class TcpClientHost : public Host
{
public:
	TcpClientHost(const Context& context, IHostEventListener *listener, unsigned short flags = JN_HOST_FLAG_NONE)
	{
		pListener = listener;
		pImpl = ::jn_host_create(context.getRawContext(), JN_HOST_TCP_CLIENT, NULL, callbackDispatcher, this, flags);
	}

	TcpClientHost(const Context& context, const Address& addr, IHostEventListener *listener, unsigned short flags = JN_HOST_FLAG_NONE)
	{
		pListener = listener;
		pImpl = ::jn_host_create(context.getRawContext(), JN_HOST_TCP_CLIENT, addr.getRawAddress(), callbackDispatcher, this, flags);
	}

	// non-copyable
private:
	TcpClientHost(const TcpClientHost& other) {}
	TcpClientHost& operator=(const TcpClientHost& other) { return *this; }
};

class UdpServerHost : public Host
{
public:
	UdpServerHost(const Context& context, const Address& addr, IHostEventListener *listener, unsigned short flags = JN_HOST_FLAG_NONE)
	{
		pListener = listener;
		pImpl = ::jn_host_create(context.getRawContext(), JN_HOST_UDP_SERVER, addr.getRawAddress(), callbackDispatcher, this, flags);
	}

	// non-copyable
private:
	UdpServerHost(const UdpServerHost& other) {}
	UdpServerHost& operator=(const UdpServerHost& other) { return *this; }
};

class UdpClientHost : public Host
{
public:
	UdpClientHost(const Context& context, IHostEventListener *listener, unsigned short flags = JN_HOST_FLAG_NONE)
	{
		pListener = listener;
		pImpl = ::jn_host_create(context.getRawContext(), JN_HOST_UDP_CLIENT, NULL, callbackDispatcher, this, flags);
	}

	// non-copyable
private:
	UdpClientHost(const UdpClientHost& other) {}
	UdpClientHost& operator=(const UdpClientHost& other) { return *this; }
};

}
