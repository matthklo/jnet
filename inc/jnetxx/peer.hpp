#pragma once

#include "../jnet/peer.h"
#include "addr.hpp"

namespace jnet
{

class Peer
{
public:
	explicit Peer(JNPeer *p, bool ownPeer = true)
		: pImpl(p)
		, bOwnPeer(ownPeer)
	{
	}

	~Peer()
	{
		if (bOwnPeer)
			::jn_peer_destroy(pImpl);
		bOwnPeer = false;
	}

#ifdef JN_CPP11
	// Peer objects are non-movable
	Peer ( Peer && other ) = delete;
	Peer& operator= ( Peer && other ) = delete;
#endif

	JNEErrCodes
	send(size_t len, const char *buf)
	{
		return ::jn_peer_send(pImpl, len, buf, NULL);
	}

	/* UDP only */	
	JNEErrCodes
	sendDest(size_t len, const char *buf, const Address &udpDest)
	{
		return ::jn_peer_send(pImpl, len, buf, udpDest.getRawAddress());
	}

	JNEErrCodes
	recv(size_t len, char *buf)
	{
		return ::jn_peer_recv(pImpl, len, buf);
	}

	JNEErrCodes
	setSockOpt(int level, int optname, const void *optval, unsigned int optlen)
	{
		return ::jn_peer_setsockopt(pImpl, level, optname, optval, optlen);
	}

	JNEErrCodes
	cancelIo()
	{
		return ::jn_peer_cancel_io(pImpl);
	}

	JNEPeerState
	getState() const
	{
		return pImpl->state;
	}

	Address
	getLocalAddr() const
	{
		return Address(&pImpl->localaddr);
	}

	Address
	getRemoteAddr() const
	{
		return Address(&pImpl->remoteaddr);
	}

	JNEErrCodes
	disconnect()
	{
		return ::jn_peer_disconnect(pImpl);
	}

	void* getUserData() const { return pImpl->userdata; }
	void  setUserData(void *data) { pImpl->userdata = data; }

private:
	// Peer objects are non-copyable.
	Peer(const Peer & other) {}
	Peer& operator= (const Peer & other) { return *this; }

	JNPeer *pImpl;
	bool bOwnPeer;
};

}


