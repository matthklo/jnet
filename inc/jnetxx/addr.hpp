#pragma once

#include "../jnet/addr.h"
#include <string>

namespace jnet
{

class Address
{
public:
	Address()
	{
		::jn_address_clear(&mAddrData);
	}

	explicit Address(const JNAddress *rawAddr)
	{
		::jn_address_clone(&mAddrData, rawAddr);
	}

	Address(const char *host, unsigned short port, unsigned short flags = JN_ADDRESS_FLAG_NONE)
	{
		::jn_address_set(&mAddrData, host, port, flags);
	}

	static Address any(bool useIpv6 = false)
	{
		return Address(NULL, 0, useIpv6 ? (JN_ADDRESS_FLAG_PASSIVE  | JN_ADDRESS_FLAG_IPV6) : JN_ADDRESS_FLAG_PASSIVE);
	}

#ifdef JN_CPP11
	Address(const Address& other) = default;
	Address& operator= (const Address& other) = default;
	Address ( Address && other ) = default;
	Address& operator= ( Address && other ) = default;
#endif

	unsigned short
	getPort() const
	{
		return mAddrData.port; 
	};

	void
	setPort(unsigned short port)
	{
		mAddrData.port = port;
	}

	JNEIPProtocolVersion
	getIpVersion() const 
	{
		return ::jn_address_tell_ipver(&mAddrData);
	}

	std::string
	getHostName(unsigned short flags = JN_ADDRESS_FLAG_NONE) const
	{
		// The maximum length of a DNS name is 255 octets. This is spelled out in RFC 1035 section 2.3.4.
		char buf[256] = {0};
		int len = 256;
		::jn_address_get_hostname(&mAddrData, buf, &len, flags);
		return std::string(buf);
	}

	const JNAddress* getRawAddress() const { return &mAddrData; }

private:
	JNAddress mAddrData;
};

}
