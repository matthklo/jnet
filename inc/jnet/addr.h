#pragma once

#include "api.h"
#include "types.h"

JN_C_API_BEGIN

// Fill the content of JNAddress to be all zero.
JNAPI void
jn_address_clear(JNAddress *addr);

// Copy raw address data from src to dst.
JNAPI void
jn_address_clone(JNAddress *dst, const JNAddress *src);

// Extract port value from the underlying sockaddr structure and store it in addr->port.
JNAPI void
jn_address_load_port(JNAddress *addr);


// Prepare the JNAddress data with given host and port. It can be then used to create host or make connection.
// If the JNAddress data will be used to create a server host and you don't care which local interface will be used,
// then just pass 'host' as NULL and set the 'JN_ADDRESS_FLAG_PASSIVE' bit in the 'flags'.
// By default IPv4 is prefered. If you want IPv6 instead, set the 'JN_ADDRESS_FLAG_IPV6' in the 'flags'.
//
// Return value:
//   JN_EC_OK: succeed.
//   JN_EC_INVALID_PARAMS: 'addr' is NULL.
//   JN_EC_PLATFORM_ERROR: failed on name resolving.
//   JN_EC_NO_MATCH: no matching address. the 'addr' leaves un-changed.
JNAPI JNEErrCodes
jn_address_set(JNAddress *addr, const char *host, unsigned short port, unsigned short flags);


// Resolve the hostname or FQDN string from the given JNAddress data.
// If fails on resolving, return the numeric ip string instead.
// 'buf' should point to a buffer for storing.
// 'length' should carry the size of the buffer. It's content will be changed to
// the actual length of the name string.
//
// If 'JN_ADDRESS_FLAG_NUMERIC' has been set in flags, it skips name resolving and always 
// returns the numeric form of hostname.
//
// Return value:
//   JN_EC_OK: succeed.
//   JN_EC_INVALID_PARAMS: either 'buf' or 'length' is NULL.
//   JN_EC_INVALID_STATE: invalid or NULL 'addr'.
//   JN_EC_OUT_OF_MEMORY: given buffer size is too small.
//   JN_EC_PLATFORM_ERROR: fatal platform error.
JNAPI JNEErrCodes
jn_address_get_hostname(const JNAddress *addr, char *buf, int *length, unsigned short flags);


// Tell the IP protocol version from given JNAddress data.
JNAPI JNEIPProtocolVersion
jn_address_tell_ipver(const JNAddress *addr);

JN_C_API_END
