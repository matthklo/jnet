#pragma once

#include "api.h"
#include "types.h"

JN_C_API_BEGIN

JNAPI int
jn_initialize();

JNAPI void
jn_terminate();


JNAPI JNContext*
jn_context_create();

// Always return 0.
JNAPI int
jn_context_destroy(JNContext* context);


JN_C_API_END
