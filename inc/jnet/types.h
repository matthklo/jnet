#pragma once
#include <stdlib.h>

typedef enum _JNEIPProtocolVersion
{
	JN_IPPROTO_IPV4 = 0,
	JN_IPPROTO_IPV6,

	JN_IPPROTO_INVALID,
} JNEIPProtocolVersion;

typedef enum _JNEErrCodes
{
	JN_EC_OK = 0,
	JN_EC_INVALID_PARAMS = -1,
	JN_EC_INVALID_STATE = -2,
	JN_EC_OUT_OF_MEMORY = -3,
	JN_EC_PLATFORM_ERROR = -4,
	JN_EC_NO_MATCH = -5,
	JN_EC_IN_PROGRESS = -6,
	JN_EC_WRONG_HOST_TYPE = -7,
	JN_EC_SHUTTING_DOWN = -8,
} JNEErrCodes;

typedef enum _JNELogLevel
{
	JN_LL_VERBOSE = 0,
	JN_LL_DEBUG,
	JN_LL_INFO,
	JN_LL_WARN,
	JN_LL_ERROR,
} JNELogLevel;

typedef struct _JNContext
{
	// customizable memory operators
	void* (*malloc)  (size_t);
	void* (*realloc) (void*, size_t);
	void  (*free)    (void*);

	// customizable logger
	void(*logger)  (JNELogLevel level, const char* msg);
} JNContext;

// NOTE: Use malloc() & free() to either create or destroy JNAddress object.
// JNET never keeps reference on a JNAddress object passed in API.
#define JN_SOCKADDR_MAX_LENGTH (128)
typedef struct _JNAddress
{
	// Should be large enough to hold any kind of sockaddr. 
	// Google 'sockaddr_storage' for more detail.
	// 'sockaddr_storage' is 128 bytes on both Linux & Windows platforms.
	char           sockaddr[JN_SOCKADDR_MAX_LENGTH];
	unsigned int   socklen;
	unsigned short port;
} JNAddress;

#define JN_ADDRESS_FLAG_NONE 0
#define JN_ADDRESS_FLAG_PASSIVE (1 << 0)
#define JN_ADDRESS_FLAG_IPV6 (1 << 1)
#define JN_ADDRESS_FLAG_NUMERIC (1 << 2)

typedef enum _JNEHostType
{
	JN_HOST_TCP_SERVER = 0,
	JN_HOST_TCP_CLIENT,
	JN_HOST_UDP_SERVER,
	JN_HOST_UDP_CLIENT,

	JN_HOST_INVALID, // keep this at the end.
} JNEHostType;

typedef struct _JNHostEvent JNHostEvent;
typedef void(*EventCallback)(JNHostEvent *);

#define JN_HOST_FLAG_NONE 0
#define JN_HOST_FLAG_IPV6_CLIENT (1<<0)

typedef struct _JNPeer JNPeer;
typedef struct _JNHost
{
	JNContext     context;
	JNPeer*       hostpeer;
	JNEHostType   type;
	void*         asio_opaque;
	EventCallback callback;
	void*         userdata;
} JNHost;

typedef enum _JNEPeerState
{
	JN_PEER_STATE_DISCONNECTED = 0,              // TCP
	JN_PEER_STATE_CONNECTING = 1,                // TCP
	JN_PEER_STATE_CONNECTED = 5,                 // TCP
	JN_PEER_STATE_DISCONNECTING = 7,             // TCP
	JN_PEER_STATE_LISTENING = 10,                // TCP (Acceptor)
	JN_PEER_UNISTATE = 11,                       // UDP
} JNEPeerState;

typedef enum _JNEShutdownDir
{
	JN_SHUTDOWN_RD,
	JN_SHUTDOWN_WR,
	JN_SHUTDOWN_BOTH,
} JNEShutdownDir;

#if defined(_WIN64)
typedef unsigned long long JNSocket;
#else
typedef int JNSocket;
#endif

typedef struct _JNAsioPeerData JNAsioPeerData;

typedef struct _JNPeer
{
	JNHost*         host;
	JNAddress       localaddr;
	JNAddress       remoteaddr;
	JNSocket        socket; // For TCP client and both UDP server and client. There will be only one socket per host.
                          // For TCP server, each peer should have it's own socket.
	JNEPeerState    state;
	void*           userdata;

	// An opaque structure used internally by jnet
	JNAsioPeerData* asio_data;
} JNPeer;

typedef enum _JNEEventType
{
	JN_EVENT_NONE = 0,
	JN_EVENT_ACCEPT,
	JN_EVENT_CONNECT,
	JN_EVENT_DISCONNECT,
	JN_EVENT_RECV,
	JN_EVENT_SEND,
} JNEEventType;

typedef struct _JNHostEvent
{
	JNEEventType    type;
	JNEErrCodes     ec;
	int             platform_ec;

	JNPeer*         peer;
	JNAddress       addr;

	// payload
	char*           in_data;
	const char*     out_data;
	unsigned int    data_len;

	// userdata passed when creating host
	void*           userdata;
} JNHostEvent;


