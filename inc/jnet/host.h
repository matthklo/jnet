#pragma once

#include "api.h"
#include "types.h"

JN_C_API_BEGIN

// Create a host: Either server or client over TCP/UDP (specified by 'type').
// For server hosts:
//     TCP: It will start listening right after creation.
//          Once a connection has been built, a JNPeer should be passed via 'cb' with JN_EVENT_CONNECT event.
//     UDP: Use 'host->hostpeer' for send/recv data. 
// For client hosts:
//     TCP: A subsequent jn_host_connect() should be called to build a connection.
//          The JNPeer should be passed via 'cb' with JN_EVENT_CONNECT event.
//     UDP: Use 'host->hostpeer' for send/recv data.
JNAPI JNHost*
jn_host_create(const JNContext *ctx, JNEHostType type, const JNAddress *addr, EventCallback cb, void *userdata, unsigned short flags);


JNAPI JNEErrCodes
jn_host_shutdown(JNHost *host);


JNAPI JNEErrCodes
jn_host_destroy(JNHost *host);


// [Async I/O]
JNAPI JNEErrCodes
jn_host_connect(JNHost *host, const JNAddress *addr, unsigned short flags);


JNAPI JNEErrCodes
jn_host_handle(JNHost *host, unsigned int interval_ms);

JN_C_API_END
