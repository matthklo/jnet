#pragma once

// This header should be include at begining of all other headers in inc/jnet (except jnet.h).
// It defines the content of JNAPI macro.
// JNAPI should be placed in the front of the name of every public interface.

#if defined(_WIN32) && defined(_MSC_VER)
#  if defined(_WINDLL)
#    define JNAPI __declspec(dllexport)
#  elif defined(JN_USE_DLL)
#    define JNAPI __declspec(dllimport)
#  else
#    define JNAPI
#  endif
#else
#  define JNAPI
#endif


#ifdef __cplusplus
#  define JN_C_API_BEGIN extern "C" {
#  define JN_C_API_END }

#  if __cplusplus > 199711L
#    define JN_CPP11
#  endif

#else
#  define JN_C_API_BEGIN
#  define JN_C_API_END
#endif
