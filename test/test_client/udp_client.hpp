#pragma once

#include <jnetxx/jnet.hpp>
#include "../test_server/worker.hpp"
#include "../test_server/packets.hpp"
#include "crc.h"

#include <memory>
#include <map>

void mylogger(JNELogLevel level, const char* fmt, ...);

struct UdpBuffer
{
	char buf[2048];
	unsigned int len;
};

class UdpClient : public jnet::IHostEventListener
{
public:
	UdpClient(const jnet::Context& ctx, const std::string& host, unsigned short port, bool useIpv6 = false)
	{
		mHost = new jnet::UdpClientHost(ctx, this, useIpv6 ? JN_HOST_FLAG_IPV6_CLIENT : 0);
		mPeer = 0;

		if (!mHost)
			return;

		mDest = jnet::Address(host.c_str(), port, useIpv6 ? JN_ADDRESS_FLAG_IPV6 : 0);
		mReceivedSerial = 0;
		mExpectingChecksumCount = 0;
		mChecksum = 0;
		mOutgoingSerial = 0;
		mPeer = mHost->getHostPeer();

		mThread = new WorkerThread(mHost);
		mThread->start();

		mPeer->recv(2048, mRecvBuf.buf);
		performTestLocked();
	}

	~UdpClient()
	{
		if (!mHost)
			return;

		mHost->shutdown();
		delete mThread;

		delete mHost;
	}

	void onEvent(jnet::Peer *peer, jnet::HostEvent *evt)
	{
		std::lock_guard<std::mutex> lg(mCallbackMutex);

		switch (evt->type)
		{
		case JN_EVENT_SEND:
			if (evt->ec != JN_EC_OK)
			{
				mylogger(JN_LL_ERROR, "JN_EVENT_SEND: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			else
			{
				mylogger(JN_LL_DEBUG, "===> Packet Sent.");
				std::map<void*, UdpBuffer*>::iterator it = mOutgoingBuffers.find((void*)evt->out_data);
				if (it != mOutgoingBuffers.end())
				{
					delete it->second;
					mOutgoingBuffers.erase((void*)evt->out_data);
				}
			}
			break;

		case JN_EVENT_RECV:
			if (evt->ec == JN_EC_OK)
			{
				mylogger(JN_LL_DEBUG, "<=== Packet Received.");
			
				std::unique_ptr<TestPacketBase> pkt_base(inflatePacket(evt->in_data));
				if (pkt_base->serial != (mReceivedSerial + 1))
				{
					mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Packet misorder !!");
					peer->disconnect();
					break;
				}
				mReceivedSerial++;

				bool continue_recv = true;
				switch (pkt_base->id)
				{
				case PKTID_HASHIT_ACK:
				{
					TestPacketHashItAck *ack = dynamic_cast<TestPacketHashItAck*>(pkt_base.get());
					if (ack->Checksum != mChecksum)
					{
						mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Checksum mismatch !!");
						peer->disconnect();
					}
					if (--mExpectingChecksumCount == 0)
					{
						mylogger(JN_LL_INFO, "JN_EVENT_RECV: Hash result received.");
						performTestLocked();
					}
				}
				break;
				case PKTID_DROPME_ACK:
				{
					TestPacketDropMeAck *ack = dynamic_cast<TestPacketDropMeAck*>(pkt_base.get());
					mylogger(JN_LL_INFO, "JN_EVENT_RECV: Drop-me acked.");
					continue_recv = false;
				}
				break;
				default:
					mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Uncognizable packet.");
					peer->disconnect();
					continue_recv = false;
					break;
				}

				if (continue_recv)
				{
					mPeer->recv(2048, mRecvBuf.buf);
				}
			}
			else
			{
				mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			break;

		default:
			mylogger(JN_LL_ERROR, "onEvent: Unknown event type.");
			break;
		}
	}

	void performTestLocked()
	{
		mChecksum = 0;

		bool multi = (rand() % 5 == 1) ? true : false;
		if (multi)
		{
			int senttimes = multi ? (rand() % 5 + 1) : 1;
			mExpectingChecksumCount = 1;
			for (int i = 0; i <senttimes; ++i)
			{
				TestPacketHashItMulti pkt;
				pkt.serial = ++mOutgoingSerial;
				pkt.Finish = (i == (senttimes - 1));

				pkt.DataLength = 20 + rand() % 181;  // 20 ~ 200
				for (unsigned int j = 0; j<pkt.DataLength; ++j)
					pkt.Data[j] = rand() & 0xFF;
				mChecksum = crc_sum(pkt.Data, pkt.DataLength, mChecksum);

				UdpBuffer *b = new UdpBuffer;
				mOutgoingBuffers.insert(std::make_pair((void*)b->buf, b));
				b->len = pkt.serialize(b->buf);
				mPeer->sendDest(b->len, b->buf, mDest);
			}
			mylogger(JN_LL_DEBUG, "[ Request: MULTI %d times ]", senttimes);
		}
		else
		{
			mExpectingChecksumCount = (rand() % 5 + 1);
			TestPacketHashIt pkt;
			pkt.serial = ++mOutgoingSerial;
			pkt.RepeatResponse = mExpectingChecksumCount;

			pkt.DataLength = 20 + rand() % 181;  // 20 ~ 200
			for (unsigned int j = 0; j<pkt.DataLength; ++j)
				pkt.Data[j] = rand() & 0xFF;
			mChecksum = crc_sum(pkt.Data, pkt.DataLength, mChecksum);

			UdpBuffer *b = new UdpBuffer;
			mOutgoingBuffers.insert(std::make_pair((void*)b->buf, b));
			b->len = pkt.serialize(b->buf);
			mPeer->sendDest(b->len, b->buf, mDest);
			mylogger(JN_LL_DEBUG, "[ Request: REPEAT %d times ]", mExpectingChecksumCount);
		}
	}

private:
	int mOutgoingSerial;
	int mReceivedSerial;
	int mExpectingChecksumCount;
	unsigned int mChecksum;
	std::map<void*, UdpBuffer*> mOutgoingBuffers;
	UdpBuffer mRecvBuf;

	jnet::Peer *mPeer;
	jnet::UdpClientHost *mHost;
	WorkerThread *mThread;
	std::mutex mCallbackMutex;
	jnet::Address mDest;
};
