#pragma once

#include <jnetxx/jnet.hpp>
#include "../test_server/worker.hpp"
#include "../test_server/packets.hpp"
#include "crc.h"

#include <memory>
#include <deque>

#define FSM_STATE_WAIT_CONNECT 0
#define FSM_STATE_WAIT_PKT_LENGTH 1
#define FSM_STATE_WAIT_HASH_ACK 2
#define FSM_STATE_WAIT_DROP 3
#define FSM_STATE_ERROR 4

void mylogger(JNELogLevel level, const char* fmt, ...);

struct TcpBuffer
{
	char buf[2048];
	unsigned int len;
	unsigned int processed_len;
};

class TcpClient : public jnet::IHostEventListener
{
public:
	TcpClient(const jnet::Context& ctx, const std::string& host, unsigned short port, bool useIpv6 = false)
	{
		mHost = new jnet::TcpClientHost(ctx, this, useIpv6 ? JN_HOST_FLAG_IPV6_CLIENT : 0);
		mPeer = 0;

		if (!mHost)
			return;

		mReceivedSerial = 0;
		mExpectingChecksumCount = 0;
		mChecksum = 0;
		mOutgoingSerial = 0;
		mPktLength = 0;

		mState = FSM_STATE_WAIT_CONNECT;
		mThread = new WorkerThread(mHost);
		mThread->start();

		jnet::Address target(host.c_str(), port, useIpv6 ? JN_ADDRESS_FLAG_IPV6 : 0);
		mHost->connect(target, 0);
	}

	~TcpClient()
	{
		if (!mHost)
			return;

		mHost->shutdown();
		delete mThread;

		delete mHost;
	}

	void onEvent(jnet::Peer *peer, jnet::HostEvent *evt)
	{
		std::lock_guard<std::mutex> lg(mCallbackMutex);

		switch (evt->type)
		{
		case JN_EVENT_CONNECT:
			mylogger(JN_LL_INFO, "JN_EVENT_CONNECT: %s", evt->ec == JN_EC_OK ? "Succeed" : "Failed");
			if (evt->ec == JN_EC_OK)
			{
				mPeer = peer;
				mRecvBuf.len = 0;
				mPeer->recv(2, mRecvBuf.buf);
				performTestLocked();
			}
			break;

		case JN_EVENT_DISCONNECT:
			{
				mylogger(JN_LL_INFO, "JN_EVENT_DISCONNECT:");
			}
			break;

		case JN_EVENT_SEND:
			if (evt->ec != JN_EC_OK)
			{
				mylogger(JN_LL_ERROR, "JN_EVENT_SEND: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			else
			{
				TcpBuffer *b = mOutgoingBuffer.front();
				b->processed_len += evt->data_len;
				if (b->processed_len < b->len)
				{
					// partially sent, need another send.
					peer->send((b->len - b->processed_len), &b->buf[b->processed_len]);
					break;
				}
				mOutgoingBuffer.pop_front();
				delete b;

				sendPacketLocked();
			}
			break;

		case JN_EVENT_RECV:
			if (evt->ec == JN_EC_OK)
			{
				if (evt->data_len == 0)
				{
					mylogger(JN_LL_INFO, "JN_EVENT_RECV: Remote peer has terminated gracefully.");
					peer->disconnect();
					break;
				}

				if (mPktLength == 0)
				{
					if ((mRecvBuf.len) == 0 && (evt->data_len == 1))
					{
						// partial packet length received. need recv 1 more byte.
						mRecvBuf.len = 1;
						peer->recv(1, &mRecvBuf.buf[1]);
						break;
					}
					mPktLength = *(unsigned short*)evt->in_data;
					mRecvBuf.len = 0;
					peer->recv(mPktLength, mRecvBuf.buf);
				}
				else
				{
					mRecvBuf.len += evt->data_len;
					if (mRecvBuf.len < (unsigned int)mPktLength)
					{
						mylogger(JN_LL_DEBUG, "JN_EVENT_RECV: %u bytes in short. recv again.", (unsigned int)mPktLength - mRecvBuf.len);
						peer->recv((unsigned int)mPktLength - mRecvBuf.len, &mRecvBuf.buf[mRecvBuf.len]);
						break;
					}

					mPktLength = 0;
					std::unique_ptr<TestPacketBase> pkt_base(inflatePacket(evt->in_data));
					if (pkt_base->serial != (mReceivedSerial + 1))
					{
						mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Packet misorder !!");
						peer->disconnect();
						break;
					}
					mReceivedSerial++;

					bool continue_recv = true;
					switch (pkt_base->id)
					{
					case PKTID_HASHIT_ACK:
						{
							TestPacketHashItAck *ack = dynamic_cast<TestPacketHashItAck*>(pkt_base.get());
							if (ack->Checksum != mChecksum)
							{
								mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Checksum mismatch !!");
								peer->disconnect();
							}
							if (--mExpectingChecksumCount == 0)
							{
								mylogger(JN_LL_INFO, "JN_EVENT_RECV: Hash result received.");
								performTestLocked();
							}
						}
						break;
					case PKTID_DROPME_ACK:
						{
							TestPacketDropMeAck *ack = dynamic_cast<TestPacketDropMeAck*>(pkt_base.get());
							mylogger(JN_LL_INFO, "JN_EVENT_RECV: Drop-me acked.");
							continue_recv = false;
						}
						break;
					default:
						mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Uncognizable packet.");
						peer->disconnect();
						continue_recv = false;
						break;
					}

					if (continue_recv)
					{
						mRecvBuf.len = 0;
						mPeer->recv(2, mRecvBuf.buf);
					}
				}
			}
			else
			{
				mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			break;

		default:
			mylogger(JN_LL_ERROR, "onEvent: Unknown event type.");
			break;
		}
	}

	void performTestLocked()
	{
		if (!mOutgoingBuffer.empty())
		{
			mylogger(JN_LL_ERROR, "performTestLocked() called while mOutgoingBuffer is not empty!!");
			return;
		}

		mChecksum = 0;

		bool multi = (rand() % 3 == 1) ? true : false;		
		if (multi)
		{
			int senttimes = multi ? (rand() % 50 + 1) : 1;
			mExpectingChecksumCount = 1;
			for (int i= 0; i <senttimes; ++i)
			{
				TestPacketHashItMulti pkt;
				pkt.serial = ++mOutgoingSerial;
				pkt.Finish = (i == (senttimes-1));

				pkt.DataLength = 200 + rand()% 1801;  // 200 ~ 2000
				for (unsigned int j=0; j<pkt.DataLength; ++j)
					pkt.Data[j] = rand() & 0xFF;
				mChecksum = crc_sum(pkt.Data, pkt.DataLength, mChecksum);

				TcpBuffer *b = new TcpBuffer;
				b->len = pkt.serialize(&b->buf[2]);
				*(unsigned short*)b->buf = (unsigned short)b->len;
				b->len += 2;
				mOutgoingBuffer.push_back(b);
			}
		}
		else
		{
			mExpectingChecksumCount = (rand() % 50 + 1);
			TestPacketHashIt pkt;
			pkt.serial = ++mOutgoingSerial;
			pkt.RepeatResponse = mExpectingChecksumCount;

			pkt.DataLength = 200 + rand() % 1801;  // 200 ~ 2000
			for (unsigned int j = 0; j<pkt.DataLength; ++j)
				pkt.Data[j] = rand() & 0xFF;
			mChecksum = crc_sum(pkt.Data, pkt.DataLength, mChecksum);

			TcpBuffer *b = new TcpBuffer;
			b->len = pkt.serialize(&b->buf[2]);
			*(unsigned short*)b->buf = (unsigned short)b->len;
			b->len += 2;
			mOutgoingBuffer.push_back(b);
		}

		if (sendPacketLocked())
			mState = FSM_STATE_WAIT_HASH_ACK;
	}

	bool sendPacketLocked()
	{
		if (!mOutgoingBuffer.empty())
		{
			TcpBuffer *b = mOutgoingBuffer.front();
			b->processed_len = 0;
			mPeer->send(b->len, b->buf);
			return true;
		}
		return false;
	}

private:
	int mOutgoingSerial;
	int mReceivedSerial;
	int mExpectingChecksumCount;
	unsigned int mChecksum;
	std::deque<TcpBuffer*> mOutgoingBuffer;
	TcpBuffer mRecvBuf;
	unsigned short mPktLength;

	jnet::Peer *mPeer;
	jnet::TcpClientHost *mHost;
	WorkerThread *mThread;
	std::mutex mCallbackMutex;
	int mState;
};
