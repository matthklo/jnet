#ifdef _MSC_VER
#  define _CRT_SECURE_NO_WARNINGS
#  ifdef USE_VLD
#    include <vld.h>
#  endif
#endif

#include <iostream>
#include <mutex>
#include <stdarg.h>
#include <stdlib.h>
#include <time.h>

#ifndef _WIN32
#include <signal.h>
#endif

#include <jnetxx/jnet.hpp>
#include "xgetopt.h"

#include "tcp_client.hpp"
#include "udp_client.hpp"

std::recursive_mutex _g_logger_mutex;

void mylogger_raw(JNELogLevel level, const char* msg)
{
	std::lock_guard<std::recursive_mutex> lg(_g_logger_mutex);

	const char *tag = 0;
	switch (level)
	{
	case JN_LL_DEBUG:
		tag = "[Debug] ";
		break;
	case JN_LL_VERBOSE:
		tag = "[Verbose] ";
		break;
	case JN_LL_INFO:
		tag = "[Info] ";
		break;
	case JN_LL_WARN:
		tag = "[Warn] ";
		break;
	case JN_LL_ERROR:
		tag = "[Error] ";
		break;
	default:
		break;
	}

	std::cout << tag << msg << std::endl;
}

void mylogger(JNELogLevel level, const char* fmt, ...)
{
	va_list vaargs;
	va_start(vaargs, fmt);
	char buf[1024];
	vsprintf(buf, fmt, vaargs);
	va_end(vaargs);
	mylogger_raw(level, buf);
}


int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		std::cout << "Syntax: test_client [-P tcp|udp] [-t seconds] [-c client count] host [port]" << std::endl;
		return 0;
	}

	std::string host;
	std::string proto = "tcp";
	int clientCnt = 1;
	int durationMs = 0;
	unsigned short port = 5299;
	while (1)
	{
		int ch = xgetopt(argc, argv, "P:t:c:");
		if (-1 == ch)
			break;
		switch (ch)
		{
		case 'P':
			proto = xoptarg;
			if (proto != "tcp" && proto != "udp")
			{
				std::cout << "ERROR: \'-P\': Unknown protocol: \'" << xoptarg << "\'" << std::endl;
				return 1;
			}
			break;
		case 't':
			if (0 >= sscanf(xoptarg, "%u", &durationMs))
			{
				std::cout << "ERROR: \'-t\': Expecting a numberic argument but got \'" << xoptarg << "\'" << std::endl;
				return 1;
			}
			durationMs *= 1000;
			break;
		case 'c':
			if (0 >= sscanf(xoptarg, "%u", &clientCnt))
			{
				std::cout << "ERROR: \'-c\': Expecting a numberic argument but got \'" << xoptarg << "\'" << std::endl;
				return 1;
			}
			break;
		default:
			break;
		}
	}

	host = argv[xoptind++];
	if (xoptind < argc)
	{
		if (0 >= sscanf(argv[xoptind], "%hu", &port))
		{
			std::cout << "ERROR: Expecting a port number after host but got \'" << argv[xoptind] << "\'" << std::endl;
			return 1;
		}
	}

	srand((unsigned int)time(NULL));
#ifndef _WIN32
	signal(SIGPIPE, SIG_IGN);
#endif

	jnet::Initialize();

	jnet::Context ctx;
	ctx.setUserLoggerFunc(mylogger_raw);

	using namespace std::chrono;

	time_point<high_resolution_clock> startTime
		= high_resolution_clock::now();

	do
	{
		std::unique_ptr<TcpClient> tcpClient;
		std::unique_ptr<UdpClient> udpClient;

		if (proto == "tcp")
		{
			tcpClient.reset(new TcpClient(ctx, host.c_str(), port));
		}
		else if (proto == "udp")
		{
			udpClient.reset(new UdpClient(ctx, host.c_str(), port));
		}

		while ((durationMs == 0) || (duration_cast<milliseconds>(high_resolution_clock::now() - startTime) < milliseconds(durationMs)))
		{
			std::this_thread::sleep_for(milliseconds(200));
		}
	} while(0);

	mylogger(JN_LL_INFO, "Shutdown");
	jnet::Terminate();
	return 0;
}
