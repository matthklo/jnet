#pragma

#ifdef __cplusplus
extern "C" {
#endif

unsigned int
crc_sum(const char *buf, unsigned int len, unsigned int iv);

#ifdef __cplusplus
}
#endif
