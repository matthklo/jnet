#pragma once

#include <jnetxx/jnet.hpp>
#include "worker.hpp"
#include "packets.hpp"
#include "crc.h"
#include <map>
#include <string.h>
#include <deque>

void mylogger(JNELogLevel level, const char* fmt, ...);

struct TcpBuffer
{
	char buf[2048];
	unsigned int len;
	unsigned int processed_len;
};

struct TcpPeerData
{
	int mOutgoingSerial;
	int mReceivedSerial;
	unsigned int mChecksum;
	std::deque<TcpBuffer*> mOutgoingBuffer;
	unsigned short mPktLength;

	TcpBuffer rbuf;
};

class TcpServer : public jnet::IHostEventListener
{
public:
	TcpServer(const jnet::Context& ctx, unsigned short port, bool useIpv6 = false, unsigned int threadPoolSize = 8)
	{
		mThreadPoolSize = threadPoolSize;
		unsigned short flags = JN_ADDRESS_FLAG_PASSIVE;
		if (useIpv6)
			flags |= JN_ADDRESS_FLAG_IPV6;
		
		jnet::Address addr(NULL, port, flags);
		mHost = new jnet::TcpServerHost(ctx, addr, this, flags);

		if (!mHost)
			return;

		mThreadPool = new WorkerThread*[mThreadPoolSize];
		for (unsigned int i=0; i<mThreadPoolSize; ++i)
		{
			mThreadPool[i] = new WorkerThread(mHost);
		}
		for (unsigned int i = 0; i<mThreadPoolSize; ++i)
		{
			mThreadPool[i]->start();
		}
	}

	~TcpServer()
	{
		if (!mHost)
			return;

		mHost->shutdown();
		for (unsigned int i = 0; i<mThreadPoolSize; ++i)
		{
			delete mThreadPool[i];
		}
		delete[] mThreadPool;
		for (auto it : peers)
		{
			delete it.second; // delete buf
			delete it.first; // delete peer
		}
		peers.clear();
		delete mHost;
	}

	void onEvent(jnet::Peer *peer, jnet::HostEvent *evt)
	{
		std::lock_guard<std::mutex> lg(mCallbackMutex);

		switch (evt->type)
		{
		case JN_EVENT_ACCEPT:
			if (evt->ec == JN_EC_OK)
			{
				TcpPeerData *d = new TcpPeerData;
				d->mChecksum = 0;
				d->mOutgoingSerial = 0;
				d->mReceivedSerial = 0;
				d->mPktLength = 0;
				peers.insert(std::make_pair(peer, d));

				jnet::Address l,r;
				l = peer->getLocalAddr();
				r = peer->getRemoteAddr();

				std::string la = l.getHostName(JN_ADDRESS_FLAG_NUMERIC);
				std::string ra = r.getHostName(JN_ADDRESS_FLAG_NUMERIC);

				mylogger(JN_LL_INFO, "Connection accepted: Local %s:%u, Remote %s:%u", la.c_str(), l.getPort(), ra.c_str(), r.getPort());

				d->rbuf.len = 0;
				peer->recv(2, d->rbuf.buf);
			}
			else
			{
				mylogger(JN_LL_ERROR, "JN_EVENT_ACCEPT: Failed.");
			}
			break;
		case JN_EVENT_CONNECT:
			mylogger(JN_LL_ERROR, "JN_EVENT_CONNECT:");
			break;
		case JN_EVENT_DISCONNECT:
			{
				mylogger(JN_LL_INFO, "JN_EVENT_DISCONNECT:");
				std::map<jnet::Peer*, TcpPeerData*>::iterator it = peers.find(peer);
				if (it != peers.end())
				{
					TcpPeerData *d = it->second;
					for (std::deque<TcpBuffer*>::iterator it = d->mOutgoingBuffer.begin();
						it != d->mOutgoingBuffer.end(); ++it)
							delete *it;
					delete d;
					delete peer;
					peers.erase(peer);
				}
			}
			break;

		case JN_EVENT_SEND:
			if (evt->ec == JN_EC_OK)
			{
				mylogger(JN_LL_INFO, "JN_EVENT_SEND: OK");

				std::map<jnet::Peer*, TcpPeerData*>::iterator it = peers.find(peer);
				if (it == peers.end())
					break;
				TcpPeerData *d = it->second;
				TcpBuffer *b = d->mOutgoingBuffer.front();
				b->processed_len += evt->data_len;
				if (b->processed_len < b->len)
				{
					// partially sent, need another send.
					peer->send((b->len - b->processed_len), &b->buf[b->processed_len]);
					break;
				}
				d->mOutgoingBuffer.pop_front();
				delete b;

				sendPacketLocked(peer);
			}
			else
			{
				mylogger(JN_LL_WARN, "JN_EVENT_SEND: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			break;

		case JN_EVENT_RECV:
			if (evt->ec == JN_EC_OK)
			{
				std::map<jnet::Peer*, TcpPeerData*>::iterator it = peers.find(peer);
				if (it != peers.end())
				{
					TcpPeerData *d = it->second;

					if (evt->data_len == 0)
					{
						mylogger(JN_LL_INFO, "JN_EVENT_RECV: Remote peer has terminated gracefully.");
						peer->disconnect();
						break;
					}

					if (d->mPktLength == 0)
					{
						if ((d->rbuf.len == 0) && (evt->data_len == 1))
						{
							// partial packet length received. need recv 1 more byte.
							d->rbuf.len = 1;
							peer->recv(1, &d->rbuf.buf[1]);
							break;
						}
						
						d->mPktLength = *(unsigned short*)d->rbuf.buf;
						d->rbuf.len = 0;
						peer->recv(d->mPktLength, d->rbuf.buf);
					}
					else
					{
						d->rbuf.len += evt->data_len;
						if (d->rbuf.len < (unsigned int)d->mPktLength)
						{
							mylogger(JN_LL_DEBUG, "JN_EVENT_RECV: %u bytes in short. recv again.", (unsigned int)d->mPktLength - d->rbuf.len);
							peer->recv((unsigned int)d->mPktLength - d->rbuf.len, &d->rbuf.buf[d->rbuf.len]);
							break;
						}

						std::unique_ptr<TestPacketBase> pkt_base(inflatePacket(d->rbuf.buf));
						d->mPktLength = 0;
						if (pkt_base->serial != (d->mReceivedSerial + 1))
						{
							mylogger(JN_LL_WARN, "JN_EVENT_RECV: Packet misorder !!");
							peer->disconnect();
							break;
						}
						d->mReceivedSerial++;

						bool wasEmptyOutgoing = d->mOutgoingBuffer.empty();

						switch (pkt_base->id)
						{
						case PKTID_HASHIT:
							{
								TestPacketHashIt *pkt = dynamic_cast<TestPacketHashIt*>(pkt_base.get());
								TestPacketHashItAck ack;
								ack.Checksum = crc_sum(pkt->Data, pkt->DataLength, 0);
							
								for (int i=0; i<pkt->RepeatResponse; ++i)
								{
									ack.serial = ++(d->mOutgoingSerial);

									TcpBuffer *b = new TcpBuffer;
									b->len = ack.serialize(&b->buf[2]);
									*((unsigned short*)b->buf) = (unsigned short)b->len;
									b->len += 2;
									d->mOutgoingBuffer.push_back(b);
								}
								d->rbuf.len = 0;
								peer->recv(2, d->rbuf.buf);

								if (wasEmptyOutgoing)
									sendPacketLocked(peer);
							}
							break;

						case PKTID_HASHIT_MULTI:
							{
								TestPacketHashItMulti *pkt = dynamic_cast<TestPacketHashItMulti*>(pkt_base.get());
								d->mChecksum = crc_sum(pkt->Data, pkt->DataLength, d->mChecksum);
								if (pkt->Finish)
								{
									TestPacketHashItAck ack;
									TcpBuffer *b = new TcpBuffer;
									ack.serial = ++(d->mOutgoingSerial);
									ack.Checksum = d->mChecksum;
									b->len = ack.serialize(&b->buf[2]);
									*((unsigned short*)b->buf) = (unsigned short)b->len;
									b->len += 2;
									d->mOutgoingBuffer.push_back(b);
								
									d->mChecksum = 0;

									if (wasEmptyOutgoing)
										sendPacketLocked(peer);
								}
								d->rbuf.len = 0;
								peer->recv(2, d->rbuf.buf);
							}
							break;

						case PKTID_DROPME:
							{
								TestPacketDropMe *pkt = dynamic_cast<TestPacketDropMe*>(pkt_base.get());
								if (pkt->AckBeforeDrop)
								{
									TestPacketDropMeAck ack;
									TcpBuffer *b = new TcpBuffer;
									ack.serial = ++(d->mOutgoingSerial);
									b->len = ack.serialize(&b->buf[2]);
									*((unsigned short*)b->buf) = (unsigned short)b->len;
									b->len += 2;
									d->mOutgoingBuffer.push_back(b);
								}
								peer->disconnect();
							}
							break;

						default:
							mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Uncognizable packet.");
							peer->disconnect();
							break;
						}
					}
				}
				else
				{
					mylogger(JN_LL_ERROR, "Internal Fatal Error.");
				}
			}
			else
			{
				mylogger(JN_LL_WARN, "JN_EVENT_RECV: Failed. Disconnect the peer.");
				peer->disconnect();
			}
			break;
		default:
			mylogger(JN_LL_ERROR, "onEvent: Unknown event type.");
			break;
		}
	}

	bool sendPacketLocked(jnet::Peer *peer)
	{
		std::map<jnet::Peer*, TcpPeerData*>::iterator it = peers.find(peer);
		if (it == peers.end())
			return false;
		TcpPeerData *d = it->second;
		if (!d->mOutgoingBuffer.empty())
		{
			TcpBuffer *b = d->mOutgoingBuffer.front();
			b->processed_len = 0;
			peer->send(b->len, b->buf);
			return true;
		}
		return false;
	}

private:
	jnet::TcpServerHost *mHost;
	WorkerThread **mThreadPool;
	std::map<jnet::Peer*, TcpPeerData*> peers;
	unsigned int mThreadPoolSize;
	std::mutex mCallbackMutex;
};
