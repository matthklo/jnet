#pragma once

#include <thread>
#include <mutex>
#include <condition_variable>
#include <set>
#include <functional>

#include <jnetxx/host.hpp>

#include <stdio.h>
#include <time.h>

void mylogger(JNELogLevel level, const char* fmt, ...);

class WorkerThread
{
	struct ScheduledCallback
	{
	public:
		unsigned int Time; // in seconds
		std::function<void()> Callback;

		bool operator < (const ScheduledCallback& other) const
		{
			return Time < other.Time;
		}
	};

public:
	WorkerThread(jnet::Host *host)
	{
		mStartMutex.lock();
		pThread = new std::thread(threadBody, this, host);		
	}

	~WorkerThread()
	{
		pThread->join();
		delete pThread;
		pThread = NULL;
	}

	void start()
	{
		mStartMutex.unlock();
	}

	static void threadBody(WorkerThread *wt, jnet::Host *host)
	{
		std::lock_guard<std::mutex> lck(wt->mStartMutex);

		JNEErrCodes c;
		do
		{
			unsigned int ts = (unsigned int)::time(nullptr);
			while (true)
			{
				if (wt->mEvents.empty() || wt->mEvents.begin()->Time > ts)
					break;
				wt->mEvents.begin()->Callback();
				wt->mEvents.erase(wt->mEvents.begin());
			}
			c = host->handle(10);
		} while (c == JN_EC_NO_MATCH || c == JN_EC_OK);
		
		mylogger(JN_LL_WARN, "Worker thread exit with code: %d", (int)c);
	}

	template< typename F >
	void postEvent(F fn)
	{
		postEventDelayed(fn, 0);
	}

	template< typename F >
	void postEventDelayed(F fn, unsigned int delayedSeconds)
	{
		unsigned int ts = (unsigned int)::time(nullptr);
		postEventAtTime(fn, ts + delayedSeconds);
	}

	template< typename F >
	void postEventAtTime(F fn, unsigned int epochSeconds)
	{
		std::lock_guard<std::mutex> lck(mEventMutex);
		mEvents.insert({ epochSeconds , fn });
	}

private:

	std::thread *pThread;
	std::mutex mStartMutex;

	std::mutex mEventMutex;
	std::multiset<ScheduledCallback> mEvents;
};
