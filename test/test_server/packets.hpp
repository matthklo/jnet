#pragma once

#include <string.h>

enum EPacketCmd
{
	PKTID_HASHIT = 1,
	PKTID_HASHIT_ACK,
	PKTID_HASHIT_MULTI,
	PKTID_DROPME,
	PKTID_DROPME_ACK,
};

class TestPacketBase
{
public:
	EPacketCmd id;
	unsigned int serial;

	virtual ~TestPacketBase() {}

	virtual
	unsigned int serialize(char *buf) const
	{
		*((unsigned short*)buf) = (unsigned short)id;
		buf += sizeof(unsigned short);
		*((unsigned int*)buf) = serial;
		return sizeof(unsigned short) + sizeof(unsigned int);
	}

	virtual
	unsigned int deserialize(const char *buf)
	{
		id = (EPacketCmd)*((unsigned short*)buf);
		buf += sizeof(unsigned short);
		serial = *((unsigned int*)buf);
		return sizeof(unsigned short) + sizeof(unsigned int);
	}
};

class TestPacketHashIt : public TestPacketBase
{
public:
	TestPacketHashIt() { id = PKTID_HASHIT; DataLength = 0; RepeatResponse = 1; }

	char Data[2048];
	unsigned int DataLength;
	unsigned short RepeatResponse;

	unsigned int serialize(char *buf) const
	{
		unsigned int offset = TestPacketBase::serialize(buf);
		*(unsigned int*)&buf[offset] = DataLength;
		offset += sizeof(DataLength);
		memcpy(&buf[offset], Data, sizeof(char) * DataLength);
		offset += sizeof(char) * DataLength;
		*(unsigned short*)&buf[offset] = RepeatResponse;
		offset += sizeof(RepeatResponse);
		return offset;
	}

	unsigned int deserialize(const char *buf)
	{
		unsigned int offset = TestPacketBase::deserialize(buf);
		DataLength = *(unsigned int*)&buf[offset];
		offset += sizeof(DataLength);
		memcpy(Data, &buf[offset], sizeof(char) * DataLength);
		offset += sizeof(char) * DataLength;
		RepeatResponse = *(unsigned short*)&buf[offset];
		offset += sizeof(RepeatResponse);
		return offset;
	}
};

class TestPacketHashItMulti : public TestPacketBase
{
public:
	TestPacketHashItMulti() { id = PKTID_HASHIT_MULTI; DataLength = 0; Finish = false; }

	char Data[2048];
	unsigned int DataLength;
	bool Finish;

	unsigned int serialize(char *buf) const
	{
		unsigned int offset = TestPacketBase::serialize(buf);
		*(unsigned int*)&buf[offset] = DataLength;
		offset += sizeof(unsigned int);
		memcpy(&buf[offset], Data, sizeof(char) * DataLength);
		offset += sizeof(char) * DataLength;
		*(bool*)&buf[offset] = Finish;
		offset += sizeof(bool);
		return offset;
	}

	unsigned int deserialize(const char *buf)
	{
		unsigned int offset = TestPacketBase::deserialize(buf);
		DataLength = *(unsigned int*)&buf[offset];
		offset += sizeof(unsigned int);
		memcpy(Data, &buf[offset], sizeof(char) * DataLength);
		offset += sizeof(char) * DataLength;
		Finish = *(bool*)&buf[offset];
		offset += sizeof(bool);
		return offset;
	}
};

class TestPacketHashItAck : public TestPacketBase
{
public:
	TestPacketHashItAck() { id = PKTID_HASHIT_ACK; }

	unsigned int Checksum;

	unsigned int serialize(char *buf) const
	{
		unsigned int offset = TestPacketBase::serialize(buf);
		buf += offset;
		*(unsigned int*)buf = Checksum;
		offset += sizeof(unsigned int);
		return offset;
	}

	unsigned int deserialize(const char *buf)
	{
		unsigned int offset = TestPacketBase::deserialize(buf);
		Checksum = *(unsigned int*)&buf[offset];
		offset += sizeof (unsigned int);
		return offset;
	}
};

class TestPacketDropMe : public TestPacketBase
{
public:
	TestPacketDropMe() { id = PKTID_DROPME; AckBeforeDrop = true; }

	bool AckBeforeDrop;

	unsigned int serialize(char *buf) const
	{
		unsigned int offset = TestPacketBase::serialize(buf);
		buf += offset;
		*(bool*)buf = AckBeforeDrop;
		offset += sizeof(bool);
		return offset;
	}

	unsigned int deserialize(const char *buf)
	{
		unsigned int offset = TestPacketBase::deserialize(buf);
		AckBeforeDrop = *(bool*)&buf[offset];
		offset += sizeof(bool);
		return offset;
	}
};

class TestPacketDropMeAck : public TestPacketBase
{
public:
	TestPacketDropMeAck() { id = PKTID_HASHIT_ACK; }

	unsigned int serialize(char *buf) const
	{
		unsigned int offset = TestPacketBase::serialize(buf);
		buf += offset;
		return offset;
	}

	unsigned int deserialize(const char *buf)
	{
		unsigned int offset = TestPacketBase::deserialize(buf);
		return offset;
	}
};

static TestPacketBase*
inflatePacket(const char *buf)
{
	unsigned short pktid = *(unsigned short*)buf;
	TestPacketBase *p = 0;
	switch ((EPacketCmd)pktid)
	{
	case PKTID_HASHIT:
		p = new TestPacketHashIt;
		break;
	case PKTID_HASHIT_ACK:
		p = new TestPacketHashItAck;
		break;
	case PKTID_HASHIT_MULTI:
		p = new TestPacketHashItMulti;
		break;
	case PKTID_DROPME:
		p = new TestPacketDropMe;
		break;
	case PKTID_DROPME_ACK:
		p = new TestPacketDropMeAck;
		break;
	default:
		break;
	}

	if (p)
		p->deserialize(buf);
	return p;
}
