#pragma once

#include <jnetxx/jnet.hpp>
#include "worker.hpp"
#include "packets.hpp"
#include "crc.h"
#include <map>
#include <string.h>
#include <deque>

void mylogger(JNELogLevel level, const char* fmt, ...);

struct UdpBuffer
{
	char buf[2048];
	unsigned int len;
};

class UdpServer : public jnet::IHostEventListener
{
public:
	UdpServer(const jnet::Context& ctx, unsigned short port, bool useIpv6 = false, unsigned int threadPoolSize = 1)
	{
		mThreadPoolSize = threadPoolSize;
		unsigned short flags = JN_ADDRESS_FLAG_PASSIVE;
		if (useIpv6)
			flags |= JN_ADDRESS_FLAG_IPV6;

		jnet::Address addr(NULL, port, flags);
		mHost = new jnet::UdpServerHost(ctx, addr, this, flags);

		if (!mHost)
			return;

		mThreadPool = new WorkerThread*[mThreadPoolSize];
		for (unsigned int i = 0; i<mThreadPoolSize; ++i)
		{
			mThreadPool[i] = new WorkerThread(mHost);
		}
		for (unsigned int i = 0; i<mThreadPoolSize; ++i)
		{
			mThreadPool[i]->start();
		}

		mHost->getHostPeer()->recv(2048, rbuf.buf);
	}

	~UdpServer()
	{
		if (!mHost)
			return;

		mHost->shutdown();
		for (unsigned int i = 0; i<mThreadPoolSize; ++i)
		{
			delete mThreadPool[i];
		}
		delete[] mThreadPool;
		delete mHost;
	}

	void onEvent(jnet::Peer *peer, jnet::HostEvent *evt)
	{
		std::lock_guard<std::mutex> lg(mCallbackMutex);

		switch (evt->type)
		{
		case JN_EVENT_SEND:
			if (evt->ec == JN_EC_OK)
			{
				mylogger(JN_LL_INFO, "===> Packet Sent.");

				auto it = mOutgoingBuffers.find((void*)evt->out_data);
				if (it != mOutgoingBuffers.end())
				{
					delete it->second;
					mOutgoingBuffers.erase((void*)evt->out_data);
				}
			}
			else
			{
				mylogger(JN_LL_WARN, "JN_EVENT_SEND: Failed. Disconnect the peer.");
			}
			break;

		case JN_EVENT_RECV:
			if (evt->ec == JN_EC_OK)
			{
				mylogger(JN_LL_DEBUG, "<=== Packet Received.");
				std::unique_ptr<TestPacketBase> pkt_base(inflatePacket(rbuf.buf));
				if (pkt_base->serial != (mReceivedSerial + 1))
				{
					mylogger(JN_LL_WARN, "JN_EVENT_RECV: Packet misorder !!");
					break;
				}
				mReceivedSerial++;

				switch (pkt_base->id)
				{
				case PKTID_HASHIT:
				{
					TestPacketHashIt *pkt = dynamic_cast<TestPacketHashIt*>(pkt_base.get());
					TestPacketHashItAck ack;
					ack.Checksum = crc_sum(pkt->Data, pkt->DataLength, 0);

					for (int i = 0; i<pkt->RepeatResponse; ++i)
					{
						ack.serial = ++(mOutgoingSerial);

						UdpBuffer *b = new UdpBuffer;
						mOutgoingBuffers.insert(std::make_pair((void*)b->buf, b));
						b->len = ack.serialize(b->buf);
						peer->sendDest(b->len, b->buf, jnet::Address(&evt->addr));
					}
				}
				break;

				case PKTID_HASHIT_MULTI:
				{
					TestPacketHashItMulti *pkt = dynamic_cast<TestPacketHashItMulti*>(pkt_base.get());
					mChecksum = crc_sum(pkt->Data, pkt->DataLength, mChecksum);
					if (pkt->Finish)
					{
						TestPacketHashItAck ack;
						UdpBuffer *b = new UdpBuffer;
						mOutgoingBuffers.insert(std::make_pair((void*)b->buf, b));
						ack.serial = ++(mOutgoingSerial);
						ack.Checksum = mChecksum;
						b->len = ack.serialize(b->buf);
						peer->sendDest(b->len, b->buf, jnet::Address(&evt->addr));

						mChecksum = 0;
					}
				}
				break;

				case PKTID_DROPME:
				{
					TestPacketDropMe *pkt = dynamic_cast<TestPacketDropMe*>(pkt_base.get());
					if (pkt->AckBeforeDrop)
					{
						TestPacketDropMeAck ack;
						UdpBuffer *b = new UdpBuffer;
						mOutgoingBuffers.insert(std::make_pair((void*)b->buf, b));
						ack.serial = ++(mOutgoingSerial);
						b->len = ack.serialize(b->buf);
						peer->sendDest(b->len, b->buf, jnet::Address(&evt->addr));
					}
				}
				break;

				default:
					mylogger(JN_LL_ERROR, "JN_EVENT_RECV: Uncognizable packet.");
					break;
				}
			}
			else
			{
				mylogger(JN_LL_WARN, "JN_EVENT_RECV: Failed. Disconnect the peer.");
			}
			peer->recv(2048, rbuf.buf);
			break;
		default:
			mylogger(JN_LL_ERROR, "onEvent: Unknown event type.");
			break;
		}
	}

private:
	jnet::UdpServerHost *mHost;
	WorkerThread **mThreadPool;
	UdpBuffer rbuf;
	std::mutex mCallbackMutex;
	unsigned int mThreadPoolSize;
	std::map<void*, UdpBuffer*> mOutgoingBuffers;
	int mOutgoingSerial;
	int mReceivedSerial;
	unsigned int mChecksum;
};
